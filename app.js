var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var stylus = require('stylus');
var pg = require('pg');
var util = require('util');
var entity = require('./server/entity/');


var app = express();

var config = require('./server/config/config')[env];

//deals with server config
require('./server/config/express')(app, config);

var userAuth = require('./server/controllers/AuthController').handler(app, config, entity);

//deals with routing
require('./server/routes/')(app, entity);


module.exports.start = function(){
    //{force: true}
    entity.sequelize.sync().then(function(){
        app.listen(config.port);
    });
};
