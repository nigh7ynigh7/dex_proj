CREATE OR REPLACE FUNCTION insert_users_su() RETURNS TRIGGER AS $$
BEGIN
  insert into su (usersusers_id) values (new.users_id);
  return null;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER users_insert_su_trigger
AFTER INSERT ON users
FOR EACH ROW
EXECUTE PROCEDURE insert_users_su();
