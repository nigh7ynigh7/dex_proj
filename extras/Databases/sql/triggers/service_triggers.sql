CREATE OR REPLACE FUNCTION insert_service_su() RETURNS TRIGGER AS $$
BEGIN
  insert into su (serviceservice_id) values (new.service_id);
  return null;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER service_insert_su_trigger
AFTER INSERT ON service
FOR EACH ROW
EXECUTE PROCEDURE insert_service_su();
