create extension cube schema public;
create extension earthdistance schema public;


SELECT address.id, address.address_1 FROM address WHERE earth_box(ll_to_earth(2, 2), 40000) @> ll_to_earth(address.latitutde, address.longitude);


-- closest service: 
SELECT
    s.service_name        as "service_name",
    s.service_url         as "service_url",
    s.service_description as "service_description",
    a.address_1           as "address_1",
    a.number              as "number",
    a.longitude           as "longitude",
    a.latitude            as "latitude"
    FROM
        address as a
    JOIN address_su sa  on sa.addressaddress_id = a.address_id
    JOIN su su          on su.su_id = sa.susu_id
    JOIN service s      on s.service_id = su.serviceservice_id
    WHERE
        (a.longitude is not null) AND
        (a.latitude is not null) AND
        (sa.still_mines = true) AND
        (s.service_is_active = true) AND
        (earth_box(ll_to_earth($1, $2), $3) @> ll_to_earth(a.latitutde, a.longitude))
