alter table item_request drop column item_request_start_date;
alter table request add column request_start_date date;
alter table request alter column request_user_eval_comment_date drop not null;
alter table request alter column request_service_eval_comment_date drop not null;
alter table request add column request_payment_description varchar(100);
alter table request add column request_rejected boolean default false;



insert into request_state (request_state_description) values ('Criado'), ('Em Andamento'), ('Enviado');

select * from request_state;
select * from request;
select  * from item_request;
select * from item
