CREATE TABLE Request_state (
    request_state_id SERIAL NOT NULL,
	request_state_description varchar(30) NOT NULL,
	PRIMARY KEY (request_state_id)
);

CREATE TABLE goods (
    Itemitem_id int8 NOT NULL,
	PRIMARY KEY (Itemitem_id)
);

CREATE TABLE Agenda (
    Itemitem_id int8 NOT NULL,
	agenda_max_concurrency int2 DEFAULT 0,
	agenda_in_between_rest int2 DEFAULT 0,
	agenda_min_duration int2 DEFAULT 0,
	agenda_max_duration int4 DEFAULT 0,
	PRIMARY KEY (Itemitem_id)
);

CREATE TABLE Item_Request (
    Itemitem_id int8 NOT NULL,
	Requestrequest_id int8 NOT NULL,
	item_request_quantity int2 DEFAULT 0,
	item_request_discount float8 DEFAULT 0,
	item_requestprice_rate float8 DEFAULT 0,
	item_request_start_date date,
	PRIMARY KEY (Itemitem_id,
	Requestrequest_id)
);

CREATE TABLE Request (
    request_id BIGSERIAL NOT NULL,
	Serviceservice_id int4 NOT NULL,
	Usersusers_id int4 NOT NULL,
	request_date date NOT NULL DEFAULT now(),
	request_completion_date date,
	request_user_eval int2,
	request_user_eval_comment varchar(250),
    request_user_eval_comment_date date NOT NULL,
	request_service_eval int2,
	request_service_eval_comment varchar(250),
    request_service_eval_comment_date date NOT NULL,
	request_random_gen_for_payment int4 NOT NULL DEFAULT trunc(random() * 2147483647),
	user_random_gen_input int4,
	Request_staterequest_state_id int2,
	Addressaddress_id int4 NOT NULL,
	Addressaddress_id2 int4,
	PRIMARY KEY (request_id)
);

CREATE TABLE Item (
    item_id BIGSERIAL NOT NULL,
	item_name varchar(30) NOT NULL,
	item_is_active bool NOT NULL DEFAULT true,
	item_price_rate float8 DEFAULT 0,
	item_discount float8 DEFAULT 0,
	Serviceservice_id int4 NOT NULL,
	PRIMARY KEY (item_id)
);

CREATE TABLE comment_eval (
    SUsu_id int8 NOT NULL,
	Commentscomment_id int8 NOT NULL,
	comment_eval_points int2 NOT NULL DEFAULT 0,
	PRIMARY KEY (SUsu_id,
	Commentscomment_id)
);

CREATE TABLE Comments (
    comment_id BIGSERIAL NOT NULL,
	comment_content varchar(250) NOT NULL,
	comment_post_date date NOT NULL DEFAULT now(),
	comment_edit_date date,
	SSUsu_id int8 NOT NULL,
	RSUsu_id2 int8 NOT NULL,
	PRIMARY KEY (comment_id)
);

CREATE TABLE Friendship (
    SUsu_id int8 NOT NULL,
	SUsu_id2 int8 NOT NULL,
	friendship_date date NOT NULL DEFAULT now(),
	PRIMARY KEY (SUsu_id,
	SUsu_id2)
);

CREATE TABLE Address_SU (
    Addressaddress_id int4 NOT NULL,
	SUsu_id int8 NOT NULL,
	still_mines bool DEFAULT true,
	PRIMARY KEY (Addressaddress_id,
	SUsu_id)
);

CREATE TABLE Address (
    address_id SERIAL NOT NULL,
	zip varchar(20) NOT NULL,
	country varchar(50) NOT NULL,
	state varchar(50),
	address_1 varchar(100) NOT NULL,
	address_2 varchar(100),
	number int2 NOT NULL,
	apt_number int2,
	longitude float8,
	latitude float8,
	PRIMARY KEY (address_id)
);

CREATE TABLE SU (
    su_id BIGSERIAL NOT NULL,
	Usersusers_id int4 UNIQUE,
	Serviceservice_id int4 UNIQUE,
	PRIMARY KEY (su_id)
);

CREATE TABLE Owns (
    Usersusers_id int4 NOT NULL,
	Serviceservice_id int4 NOT NULL,
	ownership_level int2 NOT NULL DEFAULT 0,
	is_ongoing bool NOT NULL DEFAULT true,
	PRIMARY KEY (Usersusers_id,
	Serviceservice_id)
);

CREATE TABLE Service (
    service_id SERIAL NOT NULL,
	service_url varchar(30) NOT NULL,
	service_name varchar(50) NOT NULL,
	service_creation_date date NOT NULL DEFAULT now(),
	service_description varchar(250),
	service_verified bool NOT NULL DEFAULT false,
	service_is_active bool NOT NULL DEFAULT true,
	PRIMARY KEY (service_id)
);

CREATE TABLE UserType (
    usertype_id SERIAL NOT NULL,
	usertype_description varchar(30) NOT NULL,
	PRIMARY KEY (usertype_id)
);

CREATE TABLE Users (
    users_id SERIAL NOT NULL,
	users_url varchar(50) NOT NULL,
	users_email varchar(40) NOT NULL UNIQUE,
	users_name varchar(50) NOT NULL UNIQUE,
	users_password varchar(50) NOT NULL,
	users_registration_date date NOT NULL DEFAULT now(),
	users_birth_date date,
	users_verified bool DEFAULT false,
	UserTypeusertype_id int2,
	users_is_active bool NOT NULL DEFAULT true,
	user_is_deleted bool NOT NULL DEFAULT true,
	PRIMARY KEY (users_id)
);

CREATE INDEX Request_Serviceservice_id ON Request (Serviceservice_id
);

CREATE INDEX Request_Usersusers_id ON Request (Usersusers_id
);

CREATE INDEX Item_Serviceservice_id ON Item (Serviceservice_id
);

CREATE INDEX Comments_SSUsu_id ON Comments (SSUsu_id
);

CREATE INDEX Comments_RSUsu_id2 ON Comments (RSUsu_id2
);

CREATE INDEX Address_longitude ON Address (longitude
);

CREATE INDEX Address_latitude ON Address (latitude
);

CREATE UNIQUE INDEX Service_service_url ON Service (service_url
);

CREATE UNIQUE INDEX Users_users_url ON Users (users_url
);

ALTER TABLE Request ADD CONSTRAINT FKRequest889817 FOREIGN KEY (Request_staterequest_state_id) REFERENCES Request_state (request_state_id
);

ALTER TABLE Request ADD CONSTRAINT FKRequest377384 FOREIGN KEY (Addressaddress_id) REFERENCES Address (address_id
);

ALTER TABLE Request ADD CONSTRAINT FKRequest853722 FOREIGN KEY (Addressaddress_id2) REFERENCES Address (address_id
);

ALTER TABLE Owns ADD CONSTRAINT FKOwns475113 FOREIGN KEY (Usersusers_id) REFERENCES Users (users_id
);

ALTER TABLE Owns ADD CONSTRAINT FKOwns918085 FOREIGN KEY (Serviceservice_id) REFERENCES Service (service_id
);

ALTER TABLE Users ADD CONSTRAINT FKUsers392176 FOREIGN KEY (UserTypeusertype_id) REFERENCES UserType (usertype_id
);

ALTER TABLE SU ADD CONSTRAINT FKSU943830 FOREIGN KEY (Usersusers_id) REFERENCES Users (users_id
);

ALTER TABLE SU ADD CONSTRAINT FKSU386803 FOREIGN KEY (Serviceservice_id) REFERENCES Service (service_id
);

ALTER TABLE Address_SU ADD CONSTRAINT FKAddress_SU551204 FOREIGN KEY (Addressaddress_id) REFERENCES Address (address_id
);

ALTER TABLE Address_SU ADD CONSTRAINT FKAddress_SU4869 FOREIGN KEY (SUsu_id) REFERENCES SU (su_id
);

ALTER TABLE Friendship ADD CONSTRAINT FKFriendship839881 FOREIGN KEY (SUsu_id) REFERENCES SU (su_id
);

ALTER TABLE Friendship ADD CONSTRAINT FKFriendship614055 FOREIGN KEY (SUsu_id2) REFERENCES SU (su_id
);

ALTER TABLE Comments ADD CONSTRAINT FKComments11837 FOREIGN KEY (SSUsu_id) REFERENCES SU (su_id
);

ALTER TABLE Comments ADD CONSTRAINT FKComments717324 FOREIGN KEY (RSUsu_id2) REFERENCES SU (su_id
);

ALTER TABLE comment_eval ADD CONSTRAINT FKcomment_ev366426 FOREIGN KEY (SUsu_id) REFERENCES SU (su_id
);

ALTER TABLE comment_eval ADD CONSTRAINT FKcomment_ev985722 FOREIGN KEY (Commentscomment_id) REFERENCES Comments (comment_id
);

ALTER TABLE Item ADD CONSTRAINT FKItem100000 FOREIGN KEY (Serviceservice_id) REFERENCES Service (service_id
);

ALTER TABLE Item_Request ADD CONSTRAINT FKItem_Reque742737 FOREIGN KEY (Itemitem_id) REFERENCES Item (item_id
);

ALTER TABLE Item_Request ADD CONSTRAINT FKItem_Reque193099 FOREIGN KEY (Requestrequest_id) REFERENCES Request (request_id
);

ALTER TABLE Request ADD CONSTRAINT FKRequest12069 FOREIGN KEY (Serviceservice_id) REFERENCES Service (service_id
);

ALTER TABLE Request ADD CONSTRAINT FKRequest402494 FOREIGN KEY (Usersusers_id) REFERENCES Users (users_id
);

ALTER TABLE Agenda ADD CONSTRAINT FKAgenda166834 FOREIGN KEY (Itemitem_id) REFERENCES Item (item_id
);

ALTER TABLE goods ADD CONSTRAINT FKgoods430952 FOREIGN KEY (Itemitem_id) REFERENCES Item (item_id
);

alter table item add column picturename varchar(50);

alter table service add column picturename varchar(50);

alter table users add column picturename varchar(50);
