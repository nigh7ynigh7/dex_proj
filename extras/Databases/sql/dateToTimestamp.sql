alter table request alter column request_date set data type timestamp;
alter table request alter column request_start_date set data type timestamp;
alter table request alter column request_completion_date set data type timestamp;

select * from request
