var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;

var modelFunctions = {};

//will get a service based on on the type of the informed id.
var getFriendshipById = function(id) {
    return entity.Friendship.findAll({where : {UserFrom : id}, include :[
            {model : entity.User, include : [{all : true}], as : 'Friend' },
            {model : entity.User, include : [{all : true}], as : 'Befriended' }
        ]}).then(function(a){
        return a;
    });
};

var getFriendshipByIdAndType = function(id, type) {
    var comp = [];
    if (type === 'seller') {
        comp.push({model : entity.User, include : [{model : entity.Seller , required : true}], as : 'Friend' });
    }else if (type === 'buyer') {
        comp.push({model : entity.User, include : [{model : entity.Buyer , required : true}] , as : 'Friend'});
    }else {
        comp.push({all : true });
    }
    return entity.Friendship.findAll({where : {UserFrom : id}, include : comp});
};

var removeFriendship = function(whomid, whoid){
    return entity.Friendship.destroy({where : {
        UserTo : whoid,
        UserFrom : whomid
    }});
};
var createFriend = function(whomid, whoid){
    return entity.Friendship.create({UserTo : whoid, UserFrom : whomid});
};

var isAddedInList = function(whom, who){
    return entity.Friendship.find({where : {
        UserTo: who,
        UserFrom : whom
    }}).then(function(a){
        return a === null ? false : true;
    });
};

modelFunctions.createFriendship = createFriend;
modelFunctions.getFriendshipById = getFriendshipById;
modelFunctions.getFriendshipByIdAndType = getFriendshipByIdAndType;
modelFunctions.removeFriendship = removeFriendship;
modelFunctions.isAddedInList = isAddedInList;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
