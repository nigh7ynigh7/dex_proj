var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;


var modelFunctions = {};

var getAllItemAgenda = function() {
    return entity.Agenda.findAll().then();
};

var getItemAgendaById = function(id) {
    return entity.Agenda.find({where:{id:id}});
};

var createNewItemAgenda = function(id, name, price, discount, filename, description, maxdur, mindur) {
    var io = {
        itemName : name,
        SellerId : id
    };
    if (price) {
        io.itemPrice = Number(price);
    }
    if (discount) {
        io.discount = Number(discount);
    }
    if (filename) {
        io.picturename = filename;
    }
    if (description) {
        io.description = description;
    }

    var sObject = {};

    if (maxdur) {
        sObject.maxDuration = maxdur;
    }
    if (mindur) {
        sObject.minDuration = mindur;
    }
    console.log(io, sObject);
    return entity.Item.create(io).then(function(a){
        sObject.ItemId = a.id;
        return entity.Agenda.create(sObject);
    });
};

var updateItemAgenda = function(values, comps) {
    return entity.Agenda.update(values, {where:comps});
};

var UpdateItemAgendaDetails = function(sid, id, name, price, discount, filename, description, maxdur, mindur) {
    var where = {
        id : id,
        SellerId : sid
    };
    var io = {};
    if (name) {
        io.itemName = name;
    }
    if (!!price || Number(price)!== 0) {
        io.itemPrice = Number(price);
    }
    if (!!discount || Number(discount)!== 0) {
        io.discount = Number(discount);
    }
    if (filename) {
        io.picturename = filename;
    }
    if (description) {
        io.description = description;
    }

    var sObject = {};
    if (!!mindur || Number(mindur)!== 0 ) {
        sObject.minDuration = Number(mindur);
    }
    if (!!maxdur || Number(maxdur)!== 0 ) {
        sObject.maxDuration = Number(maxdur);
    }
    return entity.Item.update(io, {where: where}).then(function(a){
        return entity.Agenda.update(sObject, {where:{ItemId : id}});
    });
};

modelFunctions.getAllItemAgenda = getAllItemAgenda;
modelFunctions.getItemAgendaById = getItemAgendaById;
modelFunctions.createNewItemAgenda = createNewItemAgenda;
modelFunctions.updateItemAgenda = updateItemAgenda;
modelFunctions.UpdateItemAgendaDetails = UpdateItemAgendaDetails;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
