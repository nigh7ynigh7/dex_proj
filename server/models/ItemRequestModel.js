var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;

var modelFunctions = {};


//see all of the items in a request
var getItemByRequestId = function(id) {
    return entity.Request.find({where:{id : id}}).then(function(a){
        return a.getItems({paranoid : false, include : [entity.Agenda, entity.Product]});
    });
};

//Must Validate before insert
var createNewItemRequest = function(item, request, qntd, disc, rate, start) {
    return entity.Request.find({where: {id : request}});
};
var createRequestItemsArr = function(array) {
    return entity.ItemRequest.bulkCreate(array);
};


//this case, must join with request to see completed request


modelFunctions.getItemByRequestId = getItemByRequestId;
modelFunctions.createNewItemRequest = createNewItemRequest;
modelFunctions.createRequestItemsArr = createRequestItemsArr;


module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
