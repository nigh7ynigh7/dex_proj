var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;

var modelFunctions = {};

var getAllItemProduct = function() {
    return entity.Product.findAll().then();
};

var getItemProductById = function(id) {
    return entity.Product.find({where:{id:id}});
};

var createNewItemProduct = function(id, name, price, discount, filename, description) {
    console.log(id, name, price, discount, filename, description);
    var io = {
        itemName : name,
        SellerId : id
    };
    if (price) {
        io.itemPrice = Number(price);
    }
    if (discount) {
        io.discount = Number(discount);
    }
    if (filename) {
        io.picturename = filename;
    }
    if (description) {
        io.description = description;
    }
    return entity.Item.create(io).then(function(a){
        console.log(a);
        return entity.Product.create({ItemId : a.id});
    });
};

var updateItemProduct = function(values, comps) {
    return entity.Product.update(values, {where:comps});
};

var UpdateItemProductDetails = function(sid, id, name, price, discount, filename, description) {
    var where = {
        id : id,
        SellerId : sid
    };
    var io = {};
    if (name) {
        io.itemName = name;
    }
    if (!!price || Number(price)!== 0 ) {
        io.itemPrice = Number(price);
    }
    if (!!discount || Number(discount)!== 0 ) {
        io.discount = Number(discount);
    }
    if (filename) {
        io.picturename = filename;
    }
    if (description) {
        io.description = description;
    }
    console.log(io);
    return entity.Item.update(io, {where: where}).then(function(a){
        return entity.Product.update({}, {where:{ItemId : id}});
    });
};



modelFunctions.getAllItemProduct = getAllItemProduct;
modelFunctions.getItemProductById = getItemProductById;
modelFunctions.createNewItemProduct = createNewItemProduct;
modelFunctions.updateItemProduct = updateItemProduct;
modelFunctions.UpdateItemProductDetails = UpdateItemProductDetails;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
