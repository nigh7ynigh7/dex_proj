var util = require('util');
var Promise = require('promise');
var validation = require('../lib/Validation');

//loadEntity
var entity = null;

var modelFunctions = {};

var ph = require('../lib/passwordHashing');


var getAllSellers = function() {
    return entity.User.findAll({include:[{model:entity.Seller, required:true}]});
};

var getSellerById = function(id) {
    return entity.User.find({include:[{model:entity.Seller, required:true}], where : {id : id}});
};

var getSellerByUrl = function(url) {
    return entity.User.find({include:[{model:entity.Seller, required:true}], where : {url : url}});
};

var getSellerByUrlOrName = function(url) {
    var query = "%" + url + "%";
    return entity.User.findAll({include:[{model:entity.Seller, required:true}], where : {$or : [{url : {$iLike : query}}, {username : {$iLike : query}}]}});
};

var getSellerByNameOrUrl = function(name) {
    return entity.User.findAll({
        include:[{model:entity.Seller, required:true}],
        where : {$or : [
            { url : { $iLike : "%" + name + "%" } },
            { username : { $iLike : "%" + name + "%" } }
        ]}
    });
};

var getSellerByNameAndPassword = function(input, password) {
    var obj = {
        userpassword : password,
        isVisible : true
    };

    if (validation.validateEmail(input)) {
        obj.emil = input;
    }else {
        obj.url = input;
    }
    return entity.User.find({
        include:[{model:entity.Seller, required:true}],
        where : obj
    });
};

var createNewSeller = function(url, name, email, password, description, filename) {
    var sObject = {
        url : url,
        username : name,
        email : email,
        userpassword :  ph.encrypt(password)
    };
    var seller = {};
    if (description) {
        seller.description = description;
    }
    if (filename) {
        sObject.picturename = filename;
    }
    return entity.User.create(sObject).then(function(a){
        seller.UserId = a.id;
        return entity.Seller.create(seller);
    });
};

var removeSellerById = function(id) {
    return entity.Seller.destroy({UserId : id}).then(function(a){
        return entity.User.destroy({id : id});
    });
};

var removeSellerByUrl = function(url) {
    var id = null;
    return entity.User.find({where : {url : url}}).then(function(a){
        id = a.id;
        return entity.Seller.destroy({UserId : id});
    }).then(function(a){
        return entity.User.destroy({id : id});
    });
};

var updateSeller = function(values, comps) {
    return entity.Seller.update(values, {where:comps});
};

var findSellerByDistance = function(longitude, latitude, distanceInMeters) {
    return entity.Address.findAll({
        where : ['earth_box(ll_to_earth(?, ?), ?) @> ll_to_earth(latitude, longitude)', latitude, longitude, distanceInMeters],
        include : [{
            model : entity.User,
            required : true,
            attributes : ['username', 'url'],
            include :[{
                model : entity.Seller,
                required : true
            }]
        }]
    });
};

var updateSellerDetails = function(id, password, new_email, new_name, new_password, new_description, new_photo) {
    var compObj = {
        id : id,
        userpassword : ph.encrypt(password)
    };
    var updateValues = {};
    if (new_email) {
        updateValues.email = new_email;
    }
    if (new_name) {
        updateValues.username = new_name;
    }
    if (new_password) {
        updateValues.userpassword =  ph.encrypt(new_password);
    }
    if (new_photo) {
        updateValues.picturename = new_photo;
    }

    var sellerObject = {};

    if (new_description) {
        console.log('\n\nin if');
        sellerObject.description = new_description;
    }
    console.log('\n\nhey', 3, sellerObject);

    return entity.User.update(updateValues, {where : compObj}).then(function(a){
        return entity.Seller.update(sellerObject, {where : {UserId : id}});
    });
};

modelFunctions.getAllSellers = getAllSellers;
modelFunctions.getSellerById = getSellerById;
modelFunctions.getSellerByUrl = getSellerByUrl;
modelFunctions.createNewSeller = createNewSeller;
modelFunctions.register = createNewSeller;
modelFunctions.removeSellerById = removeSellerById;
modelFunctions.removeSellerByUrl = removeSellerByUrl;
modelFunctions.updateSeller = updateSeller;
modelFunctions.getSellerByNameAndPassword = getSellerByNameAndPassword;
modelFunctions.getSellerByUrlOrName = getSellerByUrlOrName;
modelFunctions.login = getSellerByNameAndPassword;
modelFunctions.findSellerByDistance = findSellerByDistance;
modelFunctions.getSellerByNameOrUrl = getSellerByNameOrUrl;
modelFunctions.updateSellerDetails = updateSellerDetails;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
