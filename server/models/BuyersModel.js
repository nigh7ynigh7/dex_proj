
var util = require('util');
var Promise = require('promise');
var validation = require('../lib/Validation');

//loadEntity
var entity = null;
module.exports.setEntity = function(ent){
    entity = ent;
};

var ph = require('../lib/passwordHashing');

var modelFunctions = {};

var getAllBuyers = function() {
    return entity.User.findAll({include:[{model:entity.Buyer, required:true}]});

};

var getBuyerById = function(userId) {
    return entity.User.find({include:[{model:entity.Buyer, required:true}], where : {id : id}});
};


var getBuyerByUrl = function(url) {
    return entity.User.find({include:[{model:entity.Buyer, required:true}], where : {url : url}});
};

var getBuyerByNameOrUrl = function(name) {
    return entity.User.findAll({
        include:[{model:entity.Buyer, required:true}],
        where : {$or : [
            { url : { $like : "%" + name + "%" } },
            { username : { $like : "%" + name + "%" } }
        ]}
    });
};

var getBuyerByNameAndPassword = function(input, password) {
    var obj = {
        userpassword : password,
        isVisible : true
    };

    if (validation.validateEmail(input)) {
        obj.emil = input;
    }else {
        obj.url = input;
    }
    return entity.User.find({
        include:[{model:entity.Buyer, required:true}],
        where : obj
    });
};

var createNewBuyer = function(url, name, email, password, filename) {
    console.log('help here',1 );
    var sObject = {
        url : url,
        username : name,
        email : email,
        userpassword : ph.encrypt(password)
    };
    console.log('help here',2 );
    if (filename) {
        sObject.picturename = filename;
    }
    return entity.User.create(sObject).then(function(a){
        return entity.Buyer.create({UserId : a.id});
    });
};

var removeBuyerById = function(id) {
    return entity.Seller.destroy({UserId : id}).then(function(a){
        return entity.User.destroy({id : id});
    });
};

var removeBuyerByUrl = function(url) {
    var id = null;
    return entity.User.find({where : {url : url}}).then(function(a){
        id = a.id;
        return entity.Buyer.destroy({UserId : id});
    }).then(function(a){
        return entity.User.destroy({id : id});
    });
};

var updateBuyer = function(values, comps) {
    return entity.buyer.update(values, {where:comps});
};

var updateBuyerDetails = function(id, password, new_email, new_name, new_password, new_photo) {
    var compObj = {
        id : id,
        userpassword : ph.encrypt(password)
    };
    var updateValues = {};
    if (new_email) {
        updateValues.email = new_email;
    }
    if (new_name) {
        updateValues.username = new_name;
    }
    if (new_password) {
        updateValues.userpassword = ph.encrypt(new_password);
    }
    if (new_photo) {
        updateValues.picturename = new_photo;
    }
    var buyerValues = {};
    return entity.User.update(updateValues, {where : compObj}).then(function(){
        return entity.Buyer.update(buyerValues, {where: {UserId : id}});
    });
};

modelFunctions.getAllBuyers = getAllBuyers;
modelFunctions.getBuyerById = getBuyerById;
modelFunctions.getBuyerByUrl = getBuyerByUrl;
modelFunctions.getBuyerByNameOrUrl = getBuyerByNameOrUrl;
modelFunctions.getBuyerByNameAndPassword = getBuyerByNameAndPassword;
modelFunctions.login = getBuyerByNameAndPassword;
modelFunctions.createNewBuyer = createNewBuyer;
modelFunctions.register = createNewBuyer;
modelFunctions.removeBuyerById = removeBuyerById;
modelFunctions.removeBuyerByUrl = removeBuyerByUrl;
modelFunctions.updateBuyer = updateBuyer;
modelFunctions.updateBuyerDetails = updateBuyerDetails;


module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
