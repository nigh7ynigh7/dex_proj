var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;


var modelFunctions = {};

//get request by id
var getRequestByRequestId = function(id) {
    return entity.Request.findAll({where:{id:id}});
};

//create the base of the request
var createNewRequest = function(service_id, user_id, address_id, state_id, date, payment){
    //validation should happen somewhere around here
    console.log(service_id, user_id, address_id, state_id, date, payment);
    var sObject = {
        BuyerId : user_id,
        SellerId : service_id,
        BuyerAddressId : address_id,
        RequestStateId : state_id,
        generatedNumber : Math.floor(Math.random()*10000 + 1),
    };
    if (date) {
        sObject.startDate = date;
    }
    if (payment) {
        sObject.payment = payment;
    }
    console.log(sObject);
    return entity.Request.create(sObject);
};

//simply updates a request
var updateRequest = function(values, comps) {
    return entity.Request.update(values, {where : comps});
};

//updates a request adding the following:
var addComment = function(request, comment, evaluation, type, poster){
    var buildUpdate = {};
    var where = {
        id : request
    };
    if (type === "buyer") {
        buildUpdate.userEvaluation = evaluation;
        buildUpdate.userEvaluationComment = comment;
        buildUpdate.userEvaluationDate = new Date();
        where.BuyerId = poster;
    }else {
        buildUpdate.serviceEvaluation = evaluation;
        buildUpdate.serviceEvaluationComment = comment;
        buildUpdate.serviceEvaluationDate = new Date();
        where.SellerId = poster;
    }
    console.log(where, buildUpdate);
    return entity.Request.update(buildUpdate, {
        where : where
    });
};

//confirms payment by adding the code
var confirmPaymentThroughCode= function(request, code, userid){
    return entity.Request.update({
        inputGeneratedValue : code,
        completionDate : new Date()
    }, {
        where : {
            id : request,
            generatedNumber : code,
            BuyerId : userid
        }
    });
};

var getRequestByUser = function(id, active, all){
    var comp = {id : id};
    return entity.User.find({where : comp, include : [entity.Seller, entity.Buyer]}).then(function(a){
        var getVals = [];
        if (!all) {
            if (active) getVals.push('"isRejected" = false and "completionDate" is null');
            else getVals.push('"isRejected" = true or "completionDate" is not null');
        }
        var ors = [];
            if(a.Seller) ors.push({SellerId : a.Seller.id});
            if(a.Buyer) ors.push({BuyerId : a.Buyer.id});
            if (ors.length === 0) return [];

        return entity.Request.findAll({
            where: { $and : [getVals, {$or : ors}]},
            include : [
                {model : entity.Seller, include :[{model: entity.User}]},
                {model : entity.Buyer, include :[{model: entity.User}]},
                {model : entity.Address, as : 'BuyerAddress', paranoid:false},
                {model : entity.Address, as : 'SellerAddress'}, entity.RequestState],
            order : [['createdAt', 'DESC']]});
    });
};

var getRequestById = function(id){
    return entity.Request.find({where : {id : id},
        include : [{model : entity.Buyer, include : [{model : entity.User}]},
        {model : entity.Seller, include : [{model : entity.User}]},
        {model : entity.Address, as : 'BuyerAddress', paranoid:false},
        {model : entity.RequestState}]});
};
var getCompletedRequestById = function(id){
    return entity.Request.find({
        where : {$and : [{id : id},
                ['("isRejected" = true or "completionDate" is not null)']]},
        include : [{model : entity.Buyer, include : [{model : entity.User}]},
        {model : entity.Seller, include : [{model : entity.User}]},
        {model : entity.Address, as : 'BuyerAddress', paranoid:false},
        {model : entity.RequestState}]});
};

var deleteRequestById = function(id){
    return entity.Request.destroy({where : {id : id}});
};

var getAllRequestsByUser = function(id){
    return getRequestByUser(id, null, true);
};


modelFunctions.getRequestByRequestId = getRequestByRequestId;
modelFunctions.getCompletedRequestById = getCompletedRequestById;
modelFunctions.createNewRequest = createNewRequest;
modelFunctions.updateRequest = updateRequest;
modelFunctions.addComment = addComment;
modelFunctions.confirmPaymentThroughCode = confirmPaymentThroughCode;
modelFunctions.getRequestByUser = getRequestByUser;
modelFunctions.getRequestById = getRequestById;
modelFunctions.deleteRequestById = deleteRequestById;
modelFunctions.getAllRequestsByUser = getAllRequestsByUser;


module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
