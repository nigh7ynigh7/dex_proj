var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;

var modelFunctions = {};

var createComment = function(fromid, toid, comment) {
    return entity.Comment.create({content : comment, CommenterId : fromid, CommentedId : toid});
};

var isCommentMines = function(cid, userid){
    return entity.Comment.find({id : cid, CommenterId : userid}).then(function(a){
        if (a.length === 0) {
            return false;
        }else {
            return true;
        }
    });
};

//will get a service based on on the type of the informed id.
var getCommentsByUserId = function(uid, inout) {
    var comp = {
        CommentedId : uid
    };
    return entity.Comment.findAll({
        where : comp,
        include : [ {model : entity.User, as: "Commenter"},
                    {model : entity.User, as: "Commented"}],
        order : [['createdAt', 'DESC']]});
};

var getCommentsByUserUrl = function(url, inout) {
    return entity.User.find({where : { url : url }}).then(function(a){
        return getCommentsByUserId(a.id, inout);
    });
};

var updateComment = function(content, uid, cid) {
    return eneity.Comment.update({content : content}, {
        where : {
            CommenterId : uid,
            id : cid
        }
    });
};

var removeCommentById = function(id) {
    return comment.destroy({where: {
        id : comment_id
    }});
};

var removeComment = function(comment_id, users_id){
    return comment.destroy({where: {
        id : comment_id,
        CommenterId : users_id
    }});
};

var addComment = function(user, user2, comment){
    return createComment(user, user2, comment);
};

modelFunctions.getCommentsByUserId = getCommentsByUserId;
modelFunctions.getCommentsByUserUrl = getCommentsByUserUrl;
modelFunctions.updateComment = updateComment;
modelFunctions.removeComment = removeComment;
modelFunctions.addComment = addComment;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
