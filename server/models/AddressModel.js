var util         = require('util');
var Promise      = require('promise');

//loadEntity
var entity = null;

var modelFunctions = {};

var getAllAddress = function() {
    return entity.Address.findAll();
};

var getAddressById = function(addressId) {
    return entity.Address.find({where : {id : addressId}});
};

var getAddressByIdAndUser = function(addressId, userId) {
    return entity.Address.find({where : {id : addressId, UserId : userId}}
    );
};

var createNewAddress = function(inserterId, zip, street, country, state, city, num, apt, long, lat) {
    var insertObj = {
        UserId : inserterId || null,
        address : street || null,
        number : num || null,
        aptNumber : apt || null,
        zip : zip || null,
        city : city || null,
        state : state || null,
        country : country || null,
        latitude : lat || null,
        longitude : long || null
    };
    return entity.Address.create(insertObj);
};

var fixedAddressUpdate = function(inserterId, addressid, zip, street, country, state, city, num, apt, long, lat) {
    //values to update
    var updateObject = {};
        if(street) { updateObject.address = street; }
        if(num) { updateObject.number = num; }
        if(apt) { updateObject.aptNumber = apt; }
        if(zip) { updateObject.zip = zip; }
        if(city) { updateObject.city = city; }
        if(state) { updateObject.state = state; }
        if(country) { updateObject.country = country; }
        if(lat) { updateObject.latitude = lat; }
        if(long) { updateObject.longitude = long; }
    //comparison values
    var compObject = {
        UserId : inserterId || null,
        id : addressid || null
    };
    return entity.Address.update(updateObject, {where : compObject});
};

var removeAddressById = function(id) {
    return entity.Address.destroy({where : {id : id}});
};

var removeAddressByIdAndUser = function(id, uid) {
    return entity.Address.destroy({where : {id : id, UserId : uid}});
};

var updateAddress = function(values, comps) {
    return entity.Address.update(values,{where : comps});
};

var getAddressesByUserId = function(userid) {
    return entity.Address.findAll({where : {UserId : userid}});
};


modelFunctions.getAllAddress = getAllAddress;
modelFunctions.getAddressByIdAndUser = getAddressByIdAndUser;
modelFunctions.getAddressById = getAddressById;
modelFunctions.createNewAddress = createNewAddress;
modelFunctions.removeAddressById = removeAddressById;
modelFunctions.removeAddressByIdAndUser = removeAddressByIdAndUser;
modelFunctions.updateAddress = updateAddress;
modelFunctions.getAddressesByUserId = getAddressesByUserId;
modelFunctions.fixedAddressUpdate = fixedAddressUpdate;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
