var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;

var modelFunctions = {};

var getStateById = function(value) {
    return entity.RequestState.find({where:{id:value}});
};
var getStateByDesc = function(desc) {
    return entity.RequestState.find({where:{description : desc}});
};
var getAllStates = function() {
    return entity.RequestState.findAll();
};
var getNumbersOfReqPerGivenTimeFrame = function(sellerId, requestTime){
    if (!requestTime) {
        requestTime = new Date();
    }
    if (!sellerId || sellerId < 1) {
        sellerId = 0;
    }
    return entity.Request.findAll({});
    /*
        make code that translates the following code into sequelize:
        1 Use sequelize to find all ItemRequests - AGENDA/ITEM + REQUEST
          Active requests OK?
        2 Manually sum the items obtained from the list
          -----      -----
          -----  --> -----
          -----
          -----
        3 Count all of the items
        4 Compare the overall count, check to see if it's ok (seller limit)
    */
};

modelFunctions.getStateById = getStateById;
modelFunctions.getAllStates = getAllStates;
modelFunctions.getStateByDesc = getStateByDesc;


module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
