var util = require('util');
var Promise = require('promise');
var validation = require('../lib/Validation');

var ph = require('../lib/passwordHashing');
//loadEntity
var entity = null;

var modelFunctions = {};

var getAllUsers = function() {
    return entity.User.findAll({include : [entity.Seller, entity.Buyer]});
};

var getUserById = function(userId) {
    return entity.User.find({where : {id : userId}, include : [entity.Seller, entity.Buyer]});
};

var getUserByUrl = function(url) {
    return entity.User.find({where : {url : url}, include : [entity.Seller, entity.Buyer]});
};

var createNewUser = function(url, name, email, password, filename) {
    var userObject = {
        url : url,
        username : name,
        email : email,
        userpassword : password
    };
    if (filename) {
        userObject.picturename = filename;
    }
    return entity.User.create(userObject);
};

var updateUser = function(values, comps) {
    return entity.User.update(values, {where:comp});
};

var isBuyerOrSeller = function(identifier){
    if (util.isString(identifier)) {
        return entity.User.find({url : identifier}).then(function(a){
            return a.getBuyer();
        }).then(function(a){
            if (a === null) {
                return "seller";
            }else {
                return "buyer";
            }
        });
    }else {
        return entity.User.find({id : identifier}).then(function(a){
            return a.getBuyer();
        }).then(function(a){
            if (a === null) {
                return "seller";
            }else {
                return "buyer";
            }
        });
    }
};

var getUserByNameAndPassword = function(input, password) {
    var obj = {
        isVisible : true,
        userpassword : ph.encrypt(password)
    };
    if (validation.validateEmail(input)) {
        obj.email = input;
    }else {
        obj.url = input;
    }
    return entity.User.find({
        include:[{model:entity.Seller},{model:entity.Buyer}],
        where : obj
    });
};


modelFunctions.getAllUsers = getAllUsers;
modelFunctions.getUserById = getUserById;
modelFunctions.getUserByUrl = getUserByUrl;
modelFunctions.createNewUser = createNewUser;
modelFunctions.updateUser = updateUser;
modelFunctions.getUserByNameAndPassword = getUserByNameAndPassword;
modelFunctions.login = getUserByNameAndPassword;
modelFunctions.isBuyerOrSeller = isBuyerOrSeller;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
