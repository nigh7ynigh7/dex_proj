var util = require('util');
var Promise = require('promise');

//loadEntity
var entity = null;


var modelFunctions = {};

var getAllItem = function() {
    return entity.Item.findAll();
};

var getItemByServiceIdWithFullInfo = function(id) {
    return entity.Item.findAll({include : [{model : entity.Agenda},{model : entity.Product}],where:{SellerId : id}});
};

var getItemByItemIdWithFullInfo = function(id) {
    return entity.Item.findAll({include : [{model : entity.Agenda},{model : entity.Product}],where:{id : id}});
};

var getItemsByIdsWithFullInfo = function(idStringArray) {
    console.log('\n\n dont make it bad\n');
    return entity.Item.findAll(
        {include : [entity.Agenda, entity.Product],
        where : {id : idStringArray}});
};

var getAgendasByServiceIdWithFullInfo = function(id) {
    return entity.Item.findAll({include : [{model : entity.Agenda, required:true}],where:{SellerId : id}});
};

var getGoodsByServiceIdWithFullInfo = function(id) {
    return entity.Item.findAll({include : [{model : entity.Product, required:true}],where:{SellerId : id}});
};

var getItemByName = function(name) {
    return entity.Item.findAll({
        include : [{model:entity.Seller,include:[{model:entity.User}]},{all:true}],
        where:{
            itemName : {
                $iLike : "%" + name + "%"
            }
        }
    });
};

var getItemByServiceId = function(id) {
    return entity.Item.findAll({where:{SellerId : id}});
};

var getItemById = function(id) {
    return entity.Item.find({where:{id : id},include : [{model : entity.Agenda},{model : entity.Product}, {model: entity.Seller, include : [{model: entity.User}]}]});
};

var createNewItem = function(serviceid, name, price, discount, picturename, item_description) {
    return entity.Item.create({
        SellerId : serviceid,
        itemName : name,
        itemPrice : price,
        discount : discount,
        picturename : picturename,
        description : description
    });
};

var removeItemById = function(id) {
    return entity.Item.destroy({where : {id : id}});
};

var updateItem = function(values, comps) {
    return entity.Item.update(values, {where : comps});
};

var updateMakeItemInactiveById = function(id, sid) {
    return entity.Item.destroy({where : {id : id, SellerId : sid}});
};

modelFunctions.getAllItem = getAllItem;
modelFunctions.getItemById = getItemById;
modelFunctions.getItemByName = getItemByName;
modelFunctions.getItemByServiceId = getItemByServiceId;
modelFunctions.getItemByServiceIdWithFullInfo = getItemByServiceIdWithFullInfo;
modelFunctions.createNewItem = createNewItem;
modelFunctions.removeItemById = removeItemById;
modelFunctions.updateItem = updateItem;
modelFunctions.getItemsByIdsWithFullInfo = getItemsByIdsWithFullInfo;
modelFunctions.updateMakeItemInactiveById = updateMakeItemInactiveById;
modelFunctions.getAgendasByServiceIdWithFullInfo = getAgendasByServiceIdWithFullInfo;
modelFunctions.getGoodsByServiceIdWithFullInfo = getGoodsByServiceIdWithFullInfo;
modelFunctions.getItemByItemIdWithFullInfo = getItemByItemIdWithFullInfo;


module.exports = function(ent){
    if (ent) {
        entity = ent;
        return modelFunctions;
    } else {
        throw new Error('Eyy, lmao, no entity bro');
    }
};
