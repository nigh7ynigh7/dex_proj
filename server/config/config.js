var path = require('path');
var connection = require('./connection');

var rootPath = path.join(__dirname,'/../../');

module.exports = {
    development:{
        db: connection.developmentConnectionString,
        rootPath: rootPath,
        port: process.env.PORT || 3000,
        cloudinary : {
            cloud_name : 'doodipuffery',
            api_key : '856425912181688',
            api_secret : 'GLSOjArMA6SEZf-1xriaSrhZNqw'
        },
        domain : 'https://localhost:3000'
    },
    production:{
        db : connection.productionConnectionString,
        rootPath: rootPath,
        port: process.env.PORT || 80,
        cloudinary : {
            cloud_name : 'hv7h7oitv',
            api_key : '451598174777291',
            api_secret : '6X63N0RkIYzhEeTHLoOrfLEpoaw'
        },
        domain : 'https://dexx.herokuapp.com'
    }
};
