var express = require('express'),
    stylus = require('stylus'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    passport = require('passport'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    useragent = require('express-useragent');

module.exports = function(app, config){

    var compiler = function(str, path){
        return stylus(str).set('filename',path);
    };

    app.set('views', config.rootPath + "/server/views");

    app.set('view engine', 'ejs');

    app.use(useragent.express());

    app.set('trust proxy', 'loopback');

    app.use(logger('dev'));

    app.use(cookieParser());

    app.use(bodyParser());

    app.use(session({ secret: 'find me if you can' }));

    app.use(passport.initialize());

    app.use(passport.session());

    app.use(stylus.middleware({
        src: config.rootPath + '/public',
        compile: compiler
    }));

    app.use('/',express.static(config.rootPath + 'public/'));
    app.use('/files/',express.static(config.rootPath + 'uploads/'));
};
