var util = require('util');


var ReturnObject = function(s, i, r, c){
    var ConfigObject = function(me, ty){
        this.mustNotBeEmpty = me || false;
        this.typeOfItem = ty || null;
    };
    var parseConfig = function(c){
        if (!util.isUndefined(c)) {
            if (!util.isNull(c)) {
                var mustNotBeEmpty = false;
                var typeOfItem = null;

                if ('mustNotBeEmpty' in c) {
                    mustNotBeEmpty = true;
                }
                if ('typeOfItem' in c) {
                    typeOfItem = c.typeOfItem;
                }

                return new ConfigObject(mustNotBeEmpty, typeOfItem);
            }
        }
        return new ConfigObject();
    };

    var success = s || null;
    var item    = i || null;
    var error   = r || null;
    var config  = parseConfig(c);
    
    var isEmpty = function(){
        if (item) {
            return false;
        }
        return true;
    };

    var evaluation = function(){
        if (config.mustNotBeEmpty) {
            if (isEmpty()){
                error = 'Must not be empty';
                success = false;
            }
        }
        if (config.typeOfItem) {
            if ((typeof item) !== config.typeOfItem) {
                error = 'Erong item type';
                success = false;
            }
        }
    };


    this.isSuccessful = function(){
        evaluation();
        return !!(success);
    };

    this.hasError = function(){
        evaluation();
        return !!(error);
    };

    this.getItem = function(){
        evaluation();
        return item;
    };

    this.setItem = function(i){
        item = i;
        evaluation();
    };

    this.getError = function(){
        evaluation();
        return error;
    };

    this.setError = function(e){
        error = e;
        evaluation();
    };

    this.isEmpty = isEmpty;

    this.setNewRules = function(c){
        parseConfig(c);
    };
};

module.exports = ReturnObject;
