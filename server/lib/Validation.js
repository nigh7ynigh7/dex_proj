var util = require('util');

var mustHaveAttribute = function(obj, attributeArray, options){
    if (!(attributeArray instanceof Array)) {
        return false;
    }
    if ((typeof obj) !== 'object') {
        return false;
    }
    //options
    var mustHaveAll = false;
    var mustNotHave = false;

    if (options !== undefined) {
        if ('mustHaveAll' in options) {
            if (options.mustHaveAll) {
                mustHaveAll = true;
            }
        }
        if ('mustNotHave' in options) {
            if (options.mustNotHave) {
                mustNotHave = true;
            }
        }
        if (mustHaveAll && mustNotHave) {
            return false;
        }
    }

    //actual work
    var count = 0;
    var result = false;
    var resultWithOptions = true;
    for (var i = 0; i < attributeArray.length; i++) {
        if(attributeArray[i] in obj){
            if (mustNotHave) {
                resultWithOptions = false;
                break;
            }
            else if (mustHaveAll) {
                count++;
            }
            result = true;
            resultWithOptions = true && resultWithOptions;
        }
    }

    if (mustHaveAll) {
        resultWithOptions = (count === attributeArray.length);
    }

    if (mustHaveAll || mustNotHave) {
        result = resultWithOptions;
    }

    return result;
};

var validateUrls = function(urls){
    if (util.isString(urls)) {
        if ((/^[a-zA-Z0-9\_\-]{1,30}$/g).test()) {
            return true;
        }
    }
    return false;
};

var validateEmails = function(emails){
    if (util.isString(emails)) {
        var emailRegEx = /^[\w\-\.]+@[\w\-]+?\.[\w\.]{1,}$/g;
        return emailRegEx.test(emails);
    }
    return false;
};

var validateCharLimit = function(str, lowest, limit){
    if (util.isString(str) && util.isNumber(lowest)) {

        var len = str.length;
        var rtnval = false;

        if (len >= lowest) {
            rtnval = true;
        }

        if (util.isNumber(limit)) {
            if (len <= limit ) {
                rtnval = (rtnval && true);
            }
        }

        return rtnval;
    }
    return false;
};

addressFields = function(zip, srt1, country, state, city, num, apt, long, lat){
    var result = false;
    if (util.isString(zip) && util.isString(srt1) && util.isString(country) && util.isString(state) && util.isString(city) && util.isNumber(num)) {
        result = true;
    }
    if (!util.isUndefined(apt)) {
        if (util.isNumber(apt)) {
            result = result && true;
        }else {
            result = result && false;
        }
    }
    if (!util.isUndefined(long) && util.isUndefined(lat)) {
        result = result && false;
    }
    if (!util.isUndefined(lat) && util.isUndefined(long)) {
        result = result && false;
    }
    if (result === true) {
        if (util.isNumber(lat) && util.isNumber(long)) {
            result = result && true;
        }else {
            result = result && false;
        }
    }
    return result;
};

var owner = function(ownerid, ownertype){
    var result = false;
    if (util.isNumber(ownerid)) {
        result = true;
    }
    if (util.isString(ownertype)) {
        switch (ownertype) {
            case 'users':
                result = result && true;
                break;
            case 'service':
                result = result && true;
                break;
            default:
                result = result && false;
                break;
        }
    }
    return result;
};

module.exports.mustHaveAttribute = mustHaveAttribute;

module.exports.validateUrl = validateUrls;
module.exports.validateEmail = validateEmails;
module.exports.validateCharacterLimit = validateCharLimit;
module.exports.validateAddressField = addressFields;
module.exports.validateOwnerType = owner;
