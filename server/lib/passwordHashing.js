var crypto = require('crypto');

var algorithm = 'aes256'; // or any other algorithm supported by OpenSSL
var key = 'I know i can maek yuo buttocks feel dis';
var text = 'I love kittens';

var encrypt = function(text){
    var cipher = crypto.createCipher(algorithm, key);
    var encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
    return encrypted;
};

var decrypt = function(text){
    var decipher = crypto.createDecipher(algorithm, key);
    var decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
    return decrypted;
};

var checkWithEncrypted = function(encr, nonenc){
    var cipher = crypto.createCipher(algorithm, key);
    var encrypted = cipher.update(nonenc, 'utf8', 'hex') + cipher.final('hex');
    return encrypted === nonenc;
};

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;
module.exports.checkWithEncrypted = checkWithEncrypted;
