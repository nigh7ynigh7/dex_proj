module.exports = function(app, ent){
    var uc = require('../controllers/UsersController')(ent);
    var upload = require('./Shared/multerImgs');

    app.get('/u/:url', uc.get.getUserByUrl);
    app.post('/user/edit', upload.single('editimg'), uc.post.editUser);
};
