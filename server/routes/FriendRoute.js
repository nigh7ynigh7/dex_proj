
module.exports = function(app, entity){
    var fc = require('../controllers/FriendController')(entity);
    app.get('/friend/add', fc.get.addFriend);
    app.get('/friend/remove', fc.get.removeFriend);
};
