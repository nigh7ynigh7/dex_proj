module.exports = function(app, entity){
    var rc = require('../controllers/RequestController')(entity);
    app.post('/request/create/', rc.post.postRequest);
    app.post('/request/codeConfirmation', rc.post.confirmPayment);
    app.post('/request/changeState', rc.post.changeState);
    app.post('/request/evaluate/', rc.post.evaluate);

    app.get('/request/create/:url', rc.get.getRequestPage);
    app.get('/request/reject/:id', rc.get.rejectRequest);
    app.get('/request/evaluate/:id', rc.get.getEvalPage);
    app.get('/request/:id', rc.get.getRequestById);
};
