
module.exports = function(app, entity){
    var pc = require('../controllers/PortalController')(entity);
    app.get('/portal/', pc.get.portal);
    app.get('/portal/addresses', pc.get.addresses );
    app.get('/portal/items', pc.get.items );
    app.get('/portal/requests', pc.get.requests );
    app.get('/portal/friends', pc.get.friends );
    app.get('/portal/options', pc.get.edit );
};
