module.exports = function(app, entity){
    require('./CommentRoute')(app, entity);
    require('./PortalRoute')(app, entity);
    require('./AddressRoute')(app, entity);
    require('./ItemRoute')(app, entity);
    require('./FriendRoute')(app, entity);
    require('./UserRoute')(app, entity);
    require('./RequestRoute')(app, entity);
    require('./SearchRoute')(app, entity);
    require('./IndexRoute')(app, entity);
};
