module.exports = function(app, ent){
    var cc = require('../controllers/CommentController')(ent);
    app.post('/comment/create', cc.post.addComment);
};
