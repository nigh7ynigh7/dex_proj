module.exports = function(app, entity){
    var sc = require('../controllers/SiteController')(entity);
    var authc = require('../controllers/AuthController');
    var mp = require('../controllers/utils/msgPages');
    var upload = require('./Shared/multerImgs');

    app.get('/clearLocation', sc.get.killLocationCookie);
    app.get('/scale', sc.get.scale);
    app.get('/partials/*');
    app.get('/logout', authc.logout);
    app.get('/index', sc.get.index);
    app.get('/register', sc.get.register);
    app.get('/login', sc.get.login);
    app.get('/', sc.get.index);
    app.get('*', sc.get.notFound);

    app.post('/setMap', sc.post.setMap);
    app.post('/login', authc.login);
    app.post('/register', upload.single('regimg'), sc.post.register);

};
