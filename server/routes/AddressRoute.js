
module.exports = function(app, ent){
    var ac = require('../controllers/AddressController')(ent);
    app.get('/address/delete/:addressId', ac.get.deleteAddress);
    app.get('/address/create', ac.get.createAddress);
    app.get('/address/edit/:id', ac.get.updateAddress);
    app.post('/address/create', ac.post.createAddress);
    app.post('/address/edit', ac.post.updateAddress);
};
