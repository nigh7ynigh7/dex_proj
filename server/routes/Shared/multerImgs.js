var rootPath = require('../../config/config')[process.env.NODE_ENV].rootPath;
var multer  = require('multer');
var path = require('path');
var uuid = require('node-uuid');
var storage = multer.memoryStorage({
    // destination: function (req, file, cb) {
    //     cb(null,  rootPath + '/uploads/');
    // },
    filename: function (req, file, cb) {
        cb(null, uuid.v1() + path.extname(file.originalname));
    }
});
module.exports =  multer(
    {
        storage : storage,
        fileFilter : function(rew, file, cb){
            var filename = file.originalname;
            var allow = false;
            var ext = path.extname(filename);

            if (ext === '.jpg' || ext === '.png' || ext === '.gif') {
                allow = true;
            }
            cb(null, allow);
        }
});
