
module.exports = function(app, entity){
    var sc = require('../controllers/SearchController')(entity);
    app.get('/search', sc.get.search);
};
