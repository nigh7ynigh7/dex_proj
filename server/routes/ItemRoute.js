module.exports = function(app, ent){
    var ic = require('../controllers/ItemsController')(ent);
    var upload = require('./Shared/multerImgs');
    
    app.get('/i/:id', ic.get.getItem);
    app.get('/item/delete/:id', ic.get.delete);
    app.get('/item/create', ic.get.createItem );
    app.post('/item/create', upload.single('regimg'), ic.post.createItem );
    app.get('/item/edit/:id', ic.get.editItem );
    app.post('/item/edit/', upload.single('editimg'), ic.post.editItem );
};
