var util = require('util');
var itemModel = null, itemRequestModel = null;
//remove duplicates from array
var removeDuplicates = function(arr){
    //if not applicable
    if(!(arr instanceof Array)) return [];
    if(arr.length === 0) return [];

    var finishedArray = [];
    for (var i = 0; i < arr.length; i++) {
        if(!(arr[i].ItemId)) continue;
        var add = true;
        for (var k = 0; k < finishedArray.length; k++) {
            add = add && true;
            if(arr[i].ItemId === finishedArray[k].ItemId){
                add = add && false;
                finishedArray[k].quantity += (!!arr[i].quantity ? arr[i].quantity : 0);
            }
        }
        if(add){
            finishedArray.push({quantity : arr[i].quantity, ItemId : arr[i].ItemId});
        }
    }
    return finishedArray;
};

//split ids from array
var splitIdFromArray = function(arr){
    if(!(arr instanceof Array)) return [];
    if(arr.length === 0) return [];
    var rtn = [];
    for (var i = 0; i < arr.length; i++) {
        rtn.push(arr[i].ItemId);
    }
    return rtn;
};

//get all of the current items; -> promise
var getItems = function(strArr){
    var allow = true;
    if(!(strArr instanceof Array)) allow = false;
    var string = allow ? strArr : [0];
    return itemModel.getItemsByIdsWithFullInfo(string);
};

//validate to see if there aren't 2 or more sellers in the same list
var validateCurrentItemArray = function(arr, id){
    var answer = true; var previousId = id;
    for (var i = 0; i < arr.length; i++) {
        if(previousId !== arr[i].SellerId) answer = false;
    }
    return answer;
};

//prepares arrays for insertion
var prepInsersionArray = function(itemarr, noduparr, id){
    if(!((itemarr instanceof Array) && (noduparr instanceof Array))) return [];
    if(itemarr.length === 0 || noduparr.length === 0) return [];
    console.log(id);
    var rtnArr = [];
    for (var i = 0; i < itemarr.length; i++) {
        var insertObj = {ItemId : itemarr[i].id,
            quantity : 0,
            itemPrice : itemarr[i].itemPrice,
            discount :  itemarr[i].discount,
            RequestId : id
        };
        var allow = false;
        for (var k = 0; k < noduparr.length; k++) {
            if (itemarr[i].id !== noduparr[k].ItemId) continue;
            
            insertObj.quantity = noduparr[k].quantity;
            allow = true;
        }
        if(allow) rtnArr.push(insertObj);
    }
    console.log(rtnArr);
    return rtnArr;
};



module.exports = function(im, irm){
    console.log(im, irm);
    itemModel = im;
    itemRequestModel = irm;

    var Functions = {
        insertItems : function(insertArray, requestId, sellerId){
            var noDup = removeDuplicates(insertArray);
            var split = splitIdFromArray(noDup);
            var allow = true;
            return getItems(split).then(function(a){
                    var itemarr = a;
                    if(!(itemarr instanceof Array)) throw new Error('Result from items is not an Array');
                    if(itemarr.length === 0) throw new Error('Result from items is an empty array');

                    console.log('\nsee what went wrong', 4);
                    allow = validateCurrentItemArray(itemarr, sellerId);
                    if (allow) return itemRequestModel.createRequestItemsArr(prepInsersionArray(itemarr, noDup, requestId));
                    console.log('\nsee what went wrong', 5);
                    throw new Error('');
                },function(a){
                    console.log('getitems  \n 2 \n', a);
                    allow = false; return;
                }
            );
        }
    };

    return Functions;
};
