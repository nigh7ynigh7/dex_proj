var util = require('util');
var sellerModel, buyersModel, requestModel, itemsModel, userModel, addressModel, friendshipModel;
var entity;
var setEntity = function(a){
    sellerModel = require('../models/SellersModel')(a);
    buyersModel = require('../models/BuyersModel')(a);
    requestModel = require('../models/RequestModel')(a);
    itemsModel = require('../models/ItemModel')(a);
    userModel = require('../models/UserModel')(a);
    addressModel = require('../models/AddressModel')(a);
    friendshipModel = require('../models/FriendshipModel')(a);
    entity = a;
};
var cloudinary = require('cloudinary');
var scale = require('./utils/zoomCookie');


var Controller = {
    utils : {
        getPageTypeForReqeust : function (value){
            switch (value) {
                case 'finished':
                    return 'reqfin';
                case 'ongoing':
                    return 'reqon';
                default:
                    return 'allreq';
            }
        }
    },
    get : {
        portal : function(req, res) {
            if (!req.user) return res.redirect('/');
            var type = '';
            if (req.user.Seller) {
                type = 'service';
            }else {
                type = 'users';
            }
            returnObject = {
                user: req.user,
                type: type,
                isLogged: true,
                page : 'home',
                scale : scale.getCookie(req)
            };
            return res.render('portal', returnObject);

        },
        addresses : function(req, res){
            if (!req.user) return res.redirect('/');
            var type;
            if (req.user.Seller) type = 'service';
                else type = 'users';
            var returnObject = {
                user: req.user,
                type: type,
                isLogged: true,
                page : 'address',
                load : [],
                scale : scale.getCookie(req)
            };
            addressModel.getAddressesByUserId(req.user.id).then(function(a){
                    returnObject.load = a;
                    }, function(a){
                        returnObject.load = [];
                    }
                ).then(function(a){
                    return res.render('portal', returnObject);
                }
            );
        },
        items : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/');
            var returnObject = {
                user: req.user,
                isLogged : true,
                type : "service",
                load : {agenda : [], goods : []},
                page : 'items',
                image : cloudinary.image,
                scale : scale.getCookie(req)
            };
            itemsModel.getAgendasByServiceIdWithFullInfo(req.user.Seller.id).then(function(a){
                    returnObject.load.agenda = a;
                    return itemsModel.getGoodsByServiceIdWithFullInfo(req.user.Seller.id);
                },function(a){
                    returnObject.load.agenda = [];
                    return itemsModel.getGoodsByServiceIdWithFullInfo(req.user.Seller.id);
                }
            ).then(function(a){
                    returnObject.load.goods = a;
                    return returnObject;
                }, function(a){
                    returnObject.load.goods = [];
                    return returnObject;
                }
            ).then(function(a){
                return res.render('portal', a);
            });
        },
        requests : function(req, res){
            if (!req.user) return res.redirect('/');
            var type = req.user.Seller ? "service" : "users";
            var id = req.user.id;
            var query = req.query.state || null;
            var returnObject = {
                user : req.user,
                isLogged : true,
                rtype : query,
                type : type,
                requests : [],
                page : 'request',
                pageNum : Number(req.query.p) >= 0 ? Number(req.query.p) : 0,
                resultLimit : req.query.limit ? Number(req.query.limit) : 30,
                scale : scale.getCookie(req)
            };
            //configure searching based on query
            var checkAll = false, checkActive = false;
            if (query === 'all' || !query) {
                checkAll = true;
            }else {
                if (query === 'ongoing') {
                    checkActive = true;
                }
            }
            //finds requests
            requestModel.getRequestByUser(id, checkActive, checkAll).then(function(a){
                    returnObject.requests = a;
                    return res.render('portal', returnObject);
                }, function(a){
                    console.log(a);
                    return res.redirect('/portal/');
                }
            );
        },
        edit : function(req, res){
            if (!req.user) return res.redirect('/');
            var type = req.user.Seller ? "service" : "users";
            var id = req.user.id;
            var returnObject = {
                isLogged : true,
                type : type,
                user : req.user,
                page : "edit",
                image : cloudinary.image,
                scale : scale.getCookie(req)
            };
            return res.render('portal', returnObject);
        },
        friends : function(req, res){
            if (!req.user) return res.redirect('/');
            var returnObject = {
                user : req.user,
                isLogged : true,
                page : "friends",
                load: {
                    service : [],
                    users : []
                },
                type : req.user.Seller ? "service" : "users",
                image : cloudinary.image,
                scale : scale.getCookie(req)
            };
            var id = req.user.id;
            friendshipModel.getFriendshipByIdAndType(id, 'buyer').then(function(a){
                    console.log(a, 1);
                    returnObject.load.users = a;
                    return friendshipModel.getFriendshipByIdAndType(id, 'seller');
                }, function(a){
                    console.log(a, 2);
                    returnObject.load.users = [];
                    return friendshipModel.getFriendshipByIdAndType(id, 'seller');
                }
            ).then(function(a){
                    console.log(a, 3);
                    returnObject.load.service = a; return;
                }, function(a){
                    console.log(a, 4);
                    returnObject.load.service = []; return;
                }
            ).then(function(){
                return res.render('portal', returnObject);
            });
        }
    }
};

module.exports = function(ent){
    setEntity(ent);
    return Controller;
};
