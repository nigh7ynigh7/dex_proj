var util = require('util');
// var servM = require('../models/ServiceModel');
var rootPath = require('../config/config')[process.env.NODE_ENV].rootPath;
var sellerModel, buyersModel;

var setEntity = function(a){
    sellerModel = require('../models/SellersModel')(a);
    buyersModel = require('../models/BuyersModel')(a);
};

var uploadImage = require('./utils/uploadImage');
var cloudinary = require('cloudinary');
var scale = require('./utils/zoomCookie');

var msgpage = require('./utils/msgPages');

var Controller = {
    utils : {
        prepCookie : function(long, lat, ip){
            return {
                long : long,
                lat : lat,
                ip : ip
            };
        }
    },
    get : {
        scale : function(req, res){
            if(!req.query.op) return;
            scale.modifySize(req, res, req.query.op);
            return res.redirect(req.get('referer'));
        },
        index : function(req,res){
            var returnObject = {
                isLogged : !!req.user,
                showMap : false,
                isIndex : true,
                user : req.user || null,
                scale : scale.getCookie(req)
            };
            var cook = null;
            try {
                cook = JSON.parse(req.cookies.loc);
            } catch (e) {
                cook = null;
            }
            if (!util.isNull(cook)) {
                returnObject.showMap = true;
                var long = cook.long,
                    lat = cook.lat;
                returnObject.long = long;
                returnObject.lat = lat;
                console.log(long, lat);
                sellerModel.findSellerByDistance(long, lat, 2000).then(function(a){
                        return a;
                    },function(a){
                        return [];
                    }
                ).then(function(result){
                    returnObject.services = result;
                    return res.render('index', returnObject);
                });
            }else {
                console.log('asdf');
                return res.render('index', returnObject);
            }
        },
        login : function(req, res){
            if (req.user) return res.redirect('/');

            return res.render('login', {
                isLogged : false,
                scale : scale.getCookie(req)
            });
        },
        register : function(req, res){
            if (req.user) return res.redirect('/');

            return res.render('register', {
                isLogged : false,
                scale : scale.getCookie(req)
            });
        },
        partials : function(req, res){
            res.render('../../public/' + req.params[0]);
        },
        notFound : function(req, res){
            res.render('404', {
                url : req.params[0],
                isLogged : !!(req.user),
                user : req.user || null,
                scale : scale.getCookie(req)
            });
        },
        killLocationCookie : function(req, res){
            if (!req.cookies.loc) return res.redirect('/');
            res.cookie('loc',null);
            return res.redirect('/');
        }
    },
    post : {
        setMap : function(req, res){
            var long = req.body.longitude,
                lat = req.body.latitude;
            var cookieObject = null;
            if (req.cookies.loc) {
                if (!(long.length === 0 || lat.length === 0)) {
                    cookieObject = Controller.utils.prepCookie(Number(long), Number(lat), req.ip);
                }
            }else {
                if (!(long.length === 0 || lat.length === 0)) {
                    cookieObject = Controller.utils.prepCookie(Number(long), Number(lat), req.ip);
                }
            }
            if (cookieObject) {
                res.cookie('loc', JSON.stringify(cookieObject));
            }
            return res.redirect('/');
        },
        register : function(req, res){
            if (req.user) return res.redirect('/');
                // id, password, new_email, new_name, new_password, new_description,
            var url = req.body.regurl || null;
            var name = req.body.regname || null;
            var email = req.body.regemail || null;
            var password = req.body.regpass || null;
            var description = req.body.regdesc || null;
            var regtype = req.body.regtype || null;
            var filename;

            if (req.file) {
                filename = req.file.filename;
            }
            uploadImage(req.file).then(function(a){
                    filename = a.public_id;
                    if (regtype === 'seller')
                        return sellerModel.register(url, name, email, password, description, filename);
                    else
                        return buyersModel.register(url, name, email, password, filename);
                }, function (a){
                    if (regtype === 'seller')
                        return sellerModel.register(url, name, email, password, description);
                    else
                        return buyersModel.register(url, name, email, password);
                }
            ).then(function(y){
                    // return res.redirect('/login');
                    return msgpage.send(req, res, 'success', 'Cadastro foi realizado com sucesso.', 'Sucesso', '/login');

                }, function(n){
                    // console.log('sell', 2);
                    return msgpage.send(req, res, 'danger', 'Cadastro não foi realizado. Confire os dados que você informou.', 'Cadastro não foi realizado', '/register');

                }
            );
        }
    }
};



module.exports = function(ent){
    setEntity(ent);
    return Controller;
};
