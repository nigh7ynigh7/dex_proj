var util = require('util');
var sellerModel, itemsModel;
var entity;
var setEntity = function(a){
    sellerModel = require('../models/SellersModel')(a);
    itemsModel = require('../models/ItemModel')(a);
    entity = a;
};

var cloudinary = require('cloudinary');
var scale = require('./utils/zoomCookie');

var Controller = {
    get : {
        search : function(req, res){
            var q = req.query.q;
            var type = null;
            var returnObject = {
                isLogged : req.user ? true : false,
                load : {
                    services : [],
                    items : []
                },
                query : q,
                image : cloudinary.image,
                user : req.user || null,
                servicePage : Number(req.query.sp) >= 0 ? Number(req.query.sp) : 0,
                itemPage : Number(req.query.ip) >= 0 ? Number(req.query.ip) : 0,
                recent : req.query.r === 'items' ? 'items' : 'service',
                resultLimitItem : req.query.limit ? Number(req.query.limit) : 20,
                resultLimitServices : req.query.limit ? Number(req.query.limit) : 15,
                scale : scale.getCookie(req)
            };
            if (util.isString(req.query.type)) {
                switch (req.query.type) {
                    case 'service':
                        type = 'service';
                        break;
                    case 'item':
                        type = 'item';
                        break;
                    default:
                        type = null;
                }
            }
            var allow = false;
            if(q.length === 0) return res.render('search', returnObject);
            sellerModel.getSellerByNameOrUrl(q).then(function(a){
                    allow = true;
                    returnObject.load.services = a || [];
                    return itemsModel.getItemByName(q);
                }, function(a){
                    return itemsModel.getItemByName(q);
                }
            ).then(function(a){
                    allow = allow || true;
                    returnObject.load.items = a || [];
                    return;
                }, function(a){
                    allow = allow || false;
                    return;
                }
            ).then(function(){
                    if (allow) return res.render('search', returnObject);
                    return res.redirect('/');
                }
            );
        }
    }
};

module.exports = function(ent){
    setEntity(ent);
    return Controller;
};
