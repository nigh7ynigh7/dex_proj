var util = require('util');
var friendshipModel;
var entity;
var setEntity = function(a){
    friendshipModel = require('../models/FriendshipModel')(a);
    entity = a;
};

var Controller = {
    get : {
        addFriend : function(req, res){
            if (!req.user) {
                if (req.query.url) return res.redirect('/u/'+ req.query.url);
                return res.redirect('/');
            }
            if (!req.query.id){
                if (req.query.url) return res.redirect('/u/'+ req.query.url);
                return res.redirect('/');
            }

            var toId = req.query.id;
            var toUrl = req.query.toUrl;
            var currentUserid = req.user.id;
            console.log('\n\n', toId, toUrl, currentUserid);
            friendshipModel.createFriendship(currentUserid, Number(toId)).then(function(a){
                    if (toUrl) return res.redirect('/u/' + toUrl);
                    return res.redirect('/');
                },function(a){
                    if (toUrl) return res.redirect('/u/' + toUrl);
                    return res.redirect('/');
                }
            );

        },
        removeFriend : function(req, res){
            if (!req.user) {
                if (req.query.toUrl) return res.redirect('/u/'+ req.query.toUrl);
                return res.redirect('/');
            }
            if (!req.query.id){
                if (req.query.toUrl) return res.redirect('/u/'+ req.query.toUrl);
                return res.redirect('/');
            }
            //for redirecting
            var toUrl = req.query.toUrl;
            var toId = req.query.id;
            var currentUserid = req.user.id;

            friendshipModel.removeFriendship(currentUserid, Number(toId)).then(function(a){
                    if (toUrl) return res.redirect('/u/' + toUrl);
                    return res.redirect('/');
                },function(a){
                    if (toUrl) return res.redirect('/u/' + toUrl);
                    return res.redirect('/');
                }
            );
        }
    }
};

module.exports = function(ent){
    setEntity(ent);
    return Controller;
};
