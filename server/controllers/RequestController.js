var util = require('util');
// var servM = require('../models/ServiceModel');
var rootPath = require('../config/config')[process.env.NODE_ENV].rootPath;
var itemsModel, serviceModel, requestModel, itemRequestModel, addressModel, sellerModel, requestPrepController;
var setEntity = function(a){
    itemsModel = require('../models/ItemModel')(a);
    requestModel = require('../models/RequestModel')(a);
    requestStateModel = require('../models/RequestStateModel')(a);
    itemRequestModel = require('../models/ItemRequestModel')(a);
    addressModel = require('../models/AddressModel')(a);
    sellerModel = require('../models/SellersModel')(a);
    requestPrepController = require('./RequestPrepController')(itemsModel, itemRequestModel);
};

//deals with cloud image uploading
var uploadImage = require('./utils/uploadImage');
var cloudinary = require('cloudinary');
var scale = require('./utils/zoomCookie');

var msgpage = require('./utils/msgPages');

var Controller = {
    post : {
        //this would probably be the most API-ish thing i got here.
        postRequest : function(req, res) {
            if (!req.user) return res.redirect('/');
            if (!req.user.Buyer) return res.redirect('/');
            var service = !!(req.body.service) ? Number(req.body.service) : null,
                user = !!(req.user.Buyer.id) ? req.user.Buyer.id : null,
                address = !!(req.body.address) ? req.body.address : null,
                date = !!(req.body.startDate) ? new Date(Number(req.body.startDate)) : null,
                payment = !!(req.body.payment) ? req.body.payment : null,
                url = !!(req.body.url) ? req.body.url : null,
                items = !!(req.body.items) ? req.body.items : null,
                state = null, allow = true, id = null;
            //obter estado padrão
            requestStateModel.getStateByDesc('criado').then(function(a){
                    state = a.id;
                    return requestModel.createNewRequest(service, user, address, state, date, payment);
                }, function(a){
                    allow = false; return;
                }
            ).then(function(a){
                    //criar pedido
                    id = a.id;
                    if (items.length === 0) {
                        throw new Error("Desculpe, mas as coisas não funcionam assim. Deve-se adicionar um item!");
                    }
                    return requestPrepController.insertItems(items, id, service);
                }, function(a){
                    allow = false; return;
                }
            ).then(function(a){
                    //atribuir pedido
                    if(allow)
                        return res.send({
                            success:true,
                            redirect:'/request/'+id});
                    if (id) requestModel.deleteRequestById(id);
                    return res.send({
                        success:false,
                        redirect: url ? '/u/'+url : '/'
                    });
                }, function(a){
                    if (id) requestModel.deleteRequestById(id);
                    return res.send({
                        success:false,
                        redirect: url ? '/u/'+url : '/'
                    });
                }
            );
        },
        confirmPayment : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.user.Buyer) return res.redirect('/portal/requests');
            var code = !!req.body.confirmCode ? Number(req.body.confirmCode) : null;
            var reqid = !!req.body.request_id ? Number(req.body.request_id) : null;
            var userid = Number(req.user.Buyer.id);
            requestModel.confirmPaymentThroughCode(reqid, code, userid).then(function(a){
                    if (a[0] === 1) {
                        return msgpage.send(req, res, 'success', 'O pedido foi concluído com sucesso.', 'Pedido Concluído', '/request/'+reqid);
                    }else {
                        return msgpage.send(req, res, 'danger', 'O código informado é inválido. Verifique o seu código para ver se ele está correto!', 'Pedido não foi concluído', '/request/'+reqid);
                    }
                },function(a){
                    return msgpage.send(req, res, 'danger', 'O código informado não existe.', 'Pedido não foi concluído', '/request/'+reqid);
                }
            );

        },
        changeState : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/requests');
            var stateid = !!(req.body.stateid) ?Number(req.body.stateid) : null;
            var reqid = !!req.body.request_id ? Number(req.body.request_id) : null;
            if (!(stateid && reqid)) return res.redirect('/request/' + reqid);
            console.log(stateid, reqid);
            requestModel.updateRequest({RequestStateId : stateid}, {id : reqid, SellerId : req.user.Seller.id}).then(function(a){
                    return msgpage.send(req, res, 'success', 'O estado do pedido foi modificado com sucesso.', 'Pedido modificado', '/request/'+reqid);
                },function(a){
                    return msgpage.send(req, res, 'danger', 'Houve um erro ao executar a ação requesitada.', 'Pedido não foi modificado', '/request/'+reqid);
                }
            );
        },
        evaluate : function(req, res){
            if (!req.user) return res.redirect('/');
            var request = req.body.reqid;
            var comment = req.body.reason;
            var evaluation = req.body.grade ? Number(req.body.grade) : null;
            var type = req.body.type;
            var poster = type === 'buyer' ? req.user.Buyer.id : req.user.Seller.id ;
            console.log('\n\n grade 1');
            if(!request) return res.redirect('/portal/requests');
            console.log('\n\n grade 2');
            if(!evaluation) return res.redirect('/request/evaluate/' + request);
            console.log('\n\n grade 3');
            if(!(type === 'seller' || type === 'buyer')) return res.redirect('/request/evaluate/' + request);
            console.log('\n\n grade 4');
            requestModel.addComment(request, comment, evaluation, type, poster).then(function(a){
                    return msgpage.send(req, res, 'success', 'O usuário foi avaliado com sucesso.', 'Usuário avaliado', '/request/'+request);
                }, function(a){
                    return msgpage.send(req, res, 'danger', 'Houve um erro ao executar a ação requesitada. Confire os seus dados por favor.', 'Usuário não foi avaliado', '/request/'+request);
                }
            );
        }
    },
    get : {
        getRequestPage : function(req, res){
            if (!req.user) return res.redirect('/u/' + req.params.url);
            if (!req.user.Buyer) return res.redirect('/u/' + req.params.url);
            if (!req.params.url) return res.redirect('/');
            var returnObject = {
                isLogged : true,
                page : 'request',
                type : req.query.type === "agenda" ? "agenda" : "goods",
                load : {
                    service : {}, addresses : [], items : [],
                    user : req.user
                },
                user: req.user || null,
                scale : scale.getCookie(req)
            }; var allow = true;
            sellerModel.getSellerByUrl(req.params.url).then(function(a){
                    if(!a) allow = false;
                    returnObject.load.service = a;
                    return addressModel.getAddressesByUserId(req.user.id);
                }, function(a){
                    allow = false; return;
                }
            ).then(function(a){
                    if(a.length === 0) allow = false;
                    returnObject.load.addresses = a;
                    if (returnObject.type === "agenda") {
                        return itemsModel
                            .getAgendasByServiceIdWithFullInfo(
                                returnObject.load.service.Seller.id);
                    }else {
                        return itemsModel
                            .getGoodsByServiceIdWithFullInfo(
                                returnObject.load.service.Seller.id);
                    }
                },function(a){
                    allow = false; return;
                }
            ).then(function(a){
                    if(a.length === 0) allow = false;
                    returnObject.load.items = a;
                    return;
                },function(a){
                    allow = false; return;
                }
            ).then(function(a){
                    if (allow) return res.render('services',returnObject);
                    return res.redirect('/u/' + req.params.url);
                },function(a){
                    return res.redirect('/u/' + req.params.url);
                }
            );

        },
        getRequestById : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.params.id) return res.redirect('/portal/requests');
            var returnObject = {
                isLogged : true,
                type : req.user.Buyer ? "users" : "service",
                request : null,
                items : [],
                states : [],
                image : cloudinary.image,
                user: req.user || null,
                page : 'view',
                scale : scale.getCookie(req)
            };
            var id = Number(req.params.id);
            var allow = true;
            requestModel.getRequestById(id).then(function(a){
                    if (a.Seller.User.id === req.user.id || a.Buyer.User.id === req.user.id){
                        returnObject.request = a; return itemRequestModel.getItemByRequestId(a.id);
                    }
                    allow = false; return;
                },function(a){
                    allow = false; return;
                }
            ).then(function(a){
                    if (!(a instanceof Array)) allow = false;
                    if (allow && a.length > 0) returnObject.items = a;
                        else allow = false;
                    return requestStateModel.getAllStates();
                }
            ).then(function(a){
                    if(a.length === 0 || !a)
                        return res.redirect('/portal/requests');
                    returnObject.states = a;
                    if (allow) {return res.render('requests', returnObject);}
                        else return res.redirect('/portal/requests');
                }, function(a){
                    return res.redirect('/portal/requests');
                }
            );
        },
        getEvalPage : function(req, res){
            if(!req.user) return res.redirect('/');
            if(!req.params.id) return res.redirect('/');
            var returnObject = {
                isLogged : !!req.user,
                request : null,
                page : 'evaluate',
                type : req.user.Buyer ? 'buyer' : 'seller',
                user : req.user,
                scale : scale.getCookie(req)
            }; var allow = true;
            requestModel.getCompletedRequestById(Number(req.params.id)).then(function(a){
                    if(a === null) res.redirect('/portal/requests');
                    console.log(Number(req.params.id));
                    console.log(a);
                    if(!(a.Seller.User.id === req.user.id || a.Buyer.User.id === req.user.id)) allow = false;
                    if(a.userEvaluation)
                        if(a.Buyer.User.id === req.user.id) allow = false;
                    if(a.serviceEvaluation)
                        if(a.Seller.User.id === req.user.id) allow = false;
                    returnObject.request = a;
                    if(allow) return res.render('requests', returnObject);
                    return res.redirect('/portal/requests');
                },function(a){
                    return res.redirect('/portal/requests');
                }
            );
        },
        rejectRequest : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/requests');
            if (!req.params.id) return res.redirect('/portal/requests');
            var id = req.params.id;
            requestModel.updateRequest({isRejected:true}, {id : id, SellerId : req.user.Seller.id}).then(function(a){
                    return msgpage.send(req, res, 'success', 'O pedido foi reheitado com sucesso.', 'Pedido rejeitado', '/request/'+request);
                },function(a){
                    return msgpage.send(req, res, 'danger', 'Houve um erro ao rejeitar o pedido. Espera um pouco e tente novamente.', 'Pedido não foi rejeitado', '/request/'+request);
                }
            );
        }
    }
};

module.exports = function(ent) {
    setEntity(ent);
    return Controller;
};
