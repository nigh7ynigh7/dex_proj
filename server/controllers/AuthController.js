var util = require('util');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var handler = function(app, config, entity){
    userModel = require('../models/UserModel')(entity);
    passport.use(new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, name, password, done){
            var foundUser = null;
            var error = null;

            userModel.login(name, password).then(function(yes){
                    return yes;
                }, function(no){
                    console.log(no);
                    error = no || "Something went wrong while fetching user,";
                    return null;
                }
            ).then(function(a){
                if (error) {
                    return done(error);
                }
                if (a === null) {
                    return done(null, false);
                }else {
                    return done(null, a);
                }
            });
        })
    );

    passport.serializeUser(function(user,done){
        return done(null, user.id);
    });

    passport.deserializeUser(function(id, done){
        userModel.getUserById(id).then(function(a){
            return done(null, a);
        }, function(a){
            return done('Something went wrong!', null);
        });
    });
};

var login = function(req,res,next){
    if (req.user) {
        res.redirect({succes: true, user: req.user});
    }else {
        passport.authenticate('local', function(err, user, next) {
            if (err) return res.redirect('/login');
            if (!user) return res.redirect('/login');

            req.logIn(user, function(err) {
                if (err)  return res.redirect('/login');

                return res.redirect('/');
            });
        })(req,res,next);
    }
};

var logout = function(req, res){
    req.logout();
    return res.redirect('/');
};

module.exports.handler = handler;
module.exports.login   = login;
module.exports.logout  = logout;
