// var Promise = require('promise');
var utils = require('util');

var scale = require('./zoomCookie');
var loadMeUp = {};

var setUpRTO = function(req, res){
    var obj = {};
    obj.isLogged = !!req.user;
    obj.user = req.user;
    obj.scale = scale.getCookie(req);
    return obj;
};

var sendMsg = function(req, res, type, msg, title, redirection){
    if(!res || !msg || !type || !req || !redirection) throw new Error('The information presented here cannot be null. Please check your work.');
    var rto = setUpRTO(req, res);
    rto.msg =  msg || null;
    rto.title =  title || null;
    rto.type =  type || null;
    rto.redirect = false;
    rto.isIndex = true;
    if (redirection) {
        rto.redirect = true;
        rto.redirection = redirection;
    }
    return res.render('redirect', rto);
};


loadMeUp.send = sendMsg;

module.exports = loadMeUp;
