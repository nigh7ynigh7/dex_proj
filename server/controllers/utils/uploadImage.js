var Promise = require('promise');
var fs = require('fs');
var strm = require('stream');
var cloudinary = require('cloudinary');

cloudinary.config(require('../../config/config')[process.env.NODE_ENV].cloudinary);

module.exports = function(file){
    return new Promise(function(fulfill, reject){
        if (!file) return reject();
        console.log(file);
        var bufferStream = new strm.PassThrough();
        bufferStream.end(file.buffer);
        bufferStream.pipe(cloudinary.uploader.upload_stream(function(result){
            if (result) {
                return fulfill(result);
            }else {
                return reject();
            }
        }));
    });
};
