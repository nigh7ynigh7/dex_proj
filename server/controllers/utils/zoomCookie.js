var util = require('util');

/*
Zoom object is not a simple in out thing.
    {
        level : Number (min 1, max 3)
    }
*/

// create a cookie oject
var createCookie = function(){
    var zoom = {
        level : 0
    };
    return zoom;
};

// will check a cookie and grab its object
var checkCookie = function(req){
    var exists = false;
    if(req.cookies.zoom) exists = true;
    if(!exists) return createCookie();
    var object;
    try{
        object = JSON.parse(req.cookies.zoom);
    }catch(e){
        object = null;
    }
    return object;
};

//set cookie level
var setLevel = function(level, operation){
    if(level === 2 && operation === 'p') return 2;
    if(level === 0 && operation === 'm') return 0;
    switch(operation){
        case 'p':
            return ++level;
        case 'm':
            return --level;
        case 'r':
            return 0;
        default:
            return level;
    }
};

//cookie registering
var registerCookie = function(res, cookie){
    res.cookie('zoom',JSON.stringify(cookie));
};
// will modify the cookie, you need all of the sizes
// based on some sort of input.
// req, res, level - express (req, res), string(+ or - or .)
var modifySize = function(req, res, op){
    if(!req || !res || !op) throw new Error('Apparently, one of the important items are empty to create the cookie, all are needed.');
    var cookie = checkCookie(req);
    if (cookie === null) throw new Error('Sorry, no cookie was retrieved, could be a error from JSON.parse');
    cookie.level = setLevel(cookie.level, op);
    registerCookie(res, cookie);
    return;
};

var resetCookie = function(req, res){
    return modifySize(req, res, 'r');
};

var getCookie = function(req){
    if(!req) throw new Error('You need the request');
    return checkCookie(req);
}

module.exports.modifySize = modifySize;
module.exports.resetCookie = resetCookie;
module.exports.getCookie = getCookie;
