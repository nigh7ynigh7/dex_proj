var util = require('util');
// var servM = require('../models/ServiceModel');
var rootPath = require('../config/config')[process.env.NODE_ENV].rootPath;

var commentModel;
var entity;
var setEntity = function(a){
    commentModel = require('../models/CommentModel')(a);
    entity = a;
};

var Controller = {
    post : {
        addComment : function(req, res){
            var toUrl = !!req.body.url ? req.body.url : '';
            if (!req.user) return res.redirect('/');
            var sid = req.user.id;
            var toid = !!req.body.commentUserId ? Number(req.body.commentUserId) : null;
            var comment = req.body.commentContent || null;

            if (toid && comment) {
                commentModel.addComment(sid, toid, comment).then(function(a){
                        if (toUrl) return res.redirect('/u/' + toUrl);
                        return res.redirect('/');
                    },function(a){
                        if (toUrl) return res.redirect('/u/' + toUrl);
                        return res.redirect('/');
                    }
                );
            }else {
                if (toUrl) return res.redirect('/u/' + toUrl);
                return res.redirect('/');
            }
        }
    }
};

module.exports = function(ent){
    setEntity(ent);
    return Controller;
};
