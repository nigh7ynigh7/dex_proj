var url = require('url');
var mime = require('mime');
var util = require('util');

var config = require('../config/config')[process.env.NODE_ENV];
//deals with cloud image uploading
var uploadImage = require('./utils/uploadImage');
var cloudinary = require('cloudinary');
var scale = require('./utils/zoomCookie');


var itemsAgendaModel, itemsModel, itemsGoodsModel;
var entity;
var setEntity = function(a){
    itemsModel = require('../models/ItemModel')(a);
    itemsAgendaModel = require('../models/ItemAgendaModel')(a);
    itemsGoodsModel = require('../models/ItemProductModel')(a);
    entity = a;
};

var msgpage = require('./utils/msgPages');

var Controller = {
    get: {
        createItem : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/');
            var returnObject = {
                regType : req.query.regType || 'goods',
                isLogged : true,
                page : 'create',
                user: req.user || null,
                scale : scale.getCookie(req)
            };
            return res.render('items', returnObject);
        },
        delete : function(req, res){
            if (!req.user)return res.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/');
            var id = Number(req.params.id);
            itemsModel.removeItemById(id, req.user.id).then(function(a){
                    console.log(a);
                    return res.redirect('/portal/items');
                },function(a){
                        console.log(a);
                    return res.redirect('/portal/items');
                }
            );
        },
        getItem : function(req, res){
            var returnObject = {
                regType : req.query.regType || 'goods',
                isLogged : !!req.user,
                page : 'view',
                load : {},
                image : cloudinary.image,
                user: req.user || null,
                useragent : req.useragent,
                url : config.domain,
                scale : scale.getCookie(req)
            };
            if (!req.params.id) return res.redirect('/');
            itemsModel.getItemById(Number(req.params.id)).then(function(a){
                    if (!a) return res.redirect('/');
                    returnObject.load.item = a;
                    return res.render('items', returnObject);
                },function(a){
                    return msgpage.send(req, res, 'warning', 'O item com o id '+req.params.id+' não foi encontrado. Pode ser que ele foi deletado pelo seu criador.', 'Item não foi encontrado', '/');
                }
            );

        },
        editItem : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/');
            if (!req.params.id) return res.redirect('/portal/items');

            var id = req.params.id;
            var returnObject = {
                regType : null,
                isLogged : true,
                page : 'edit',
                load : {
                    item : {},
                },
                user: req.user || null,
                useragent : req.useragent,
                scale : scale.getCookie(req)
            };
            itemsModel.getItemById(Number(id)).then(function(a){
                    if (a) {
                        returnObject.load.item = a;

                        if (returnObject.load.item.Agenda) returnObject.regType = "agenda";
                        else returnObject.regType = "goods";

                        return res.render('items', returnObject);
                    }else {
                        return res.redirect('/portal/items');
                    }
                },function(a){
                    return res.redirect('/portal/items');
                }
            );

        }
    },
    post : {
        createItem : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/');
            var id = req.user.Seller.id;
            var type = req.body.type;
            var name = req.body.regname || null;
            var price = req.body.regprice || null;
            var discount = req.body.regdisc || null;
            var description = req.body.regdesc || null;
            var filename = null;
            if (type === "goods") {
                uploadImage(req.file).then(function(a){
                    filename = a.public_id;
                    return itemsGoodsModel.createNewItemProduct(id, name, price, discount, filename, description);
                }, function (a){
                    return itemsGoodsModel.createNewItemProduct(id, name, price, discount, filename, description);
                }).then(function(a){
                        console.log(a, 1);
                        return msgpage.send(req, res, 'success', 'O item '+a.itemName + ' foi criado com sucesso.', 'Item criado com sucesso', '/portal/items');
                    },function(a){
                        console.log(a,2 );
                        return msgpage.send(req, res, 'danger', 'Houve um erro ao criar o item. Por favor confire os seus dados.', 'Erro ao criar o item', '/portal/items');
                    }
                );
            }else if (type === "agenda") {
                var max = req.body.regmax || null,
                    min = req.body.regmin || null;
                    uploadImage(req.file).then(function(a){
                        filename = a.public_id;
                        return itemsAgendaModel.createNewItemAgenda(id, name, price, discount, filename, description, max, min);
                    }, function (a){
                        return itemsAgendaModel.createNewItemAgenda(id, name, price, discount, filename, description, max, min);
                    }).then(function(a){
                        console.log(a, 3);
                        return msgpage.send(req, res, 'success', 'O item '+a.itemName + ' foi criado com sucesso.', 'Item criado com sucesso', '/portal/items');
                    },function(a){
                        console.log(a, 4);
                        return msgpage.send(req, res, 'danger', 'Houve um erro ao criar o item. Por favor confire os seus dados.', 'Erro ao criar o item', '/item/create?regType=agenda');
                    }
                );
            }else {
                return res.redirect('/portal/');
            }

        },
        editItem : function(req, res){
            if (!req.user) return req.redirect('/');
            if (!req.user.Seller) return res.redirect('/portal/');
            var sid = req.user.Seller.id || null,
                id = Number(req.body.editid) || null,
                name = req.body.editname || null,
                rate = req.body.editprice || null,
                disc = req.body.editdisc || null,
                desc = req.body.editdesc || null,
                pic = null;
            if (req.body.type === "agenda") {
                console.log('help me', 1);
                var max = req.body.editmax || null,
                    min = req.body.editmin || null;
                uploadImage(req.file).then(function(a){
                    pic = a.public_id;
                    return itemsAgendaModel.UpdateItemAgendaDetails(sid, id, name, rate, disc, pic, desc, max, min);
                }, function (a){
                    return itemsAgendaModel.UpdateItemAgendaDetails(sid, id, name, rate, disc, pic, desc, max, min);
                }).then(function(a){
                        console.log(a, 1);
                        return msgpage.send(req, res, 'success', 'O item foi modificado com sucesso.', 'Item modificado com sucesso', '/portal/items');
                    },function(a){
                        console.log(a, 2);
                        return msgpage.send(req, res, 'danger', 'Houve um erro ao modificar o item. Por favor confire os dados.', 'Erro ao modificado o item', '/item/edit/'+id);
                    }
                );
            }else if (req.body.type === "goods") {
                console.log('help me', 2);
                uploadImage(req.file).then(function(a){
                    pic = a.public_id;
                    return itemsGoodsModel.UpdateItemProductDetails(sid, id, name, rate, disc, pic, desc);
                }, function (a){
                    return itemsGoodsModel.UpdateItemProductDetails(sid, id, name, rate, disc, pic, desc);
                }).then(function(a){
                        console.log(a, 3);
                        return msgpage.send(req, res, 'success', 'O item foi modificado com sucesso.', 'Item modificado com sucesso', '/portal/items');
                    },function(a){
                        console.log(a, 4);
                        return msgpage.send(req, res, 'danger', 'Houve um erro ao modificar o item. Por favor confire os dados.', 'Erro ao modificado o item', '/item/edit/'+id);
                    }
                );
            }else {
                console.log('help me', 3);
                return msgpage.send(req, res, 'danger', 'O que você fez comigo?', 'What?', '/item/edit/'+id);
            }
        }
    }
};

module.exports = function(ent){
    setEntity(ent);
    return Controller;
};
