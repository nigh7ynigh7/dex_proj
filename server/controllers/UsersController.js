var sellerModel, buyersModel, itemsModel, userModel, friendshipModel, commentModel;
var util = require('util');
var entity;
var setEntity = function(a) {
    sellerModel = require('../models/SellersModel')(a);
    buyersModel = require('../models/BuyersModel')(a);
    itemsModel = require('../models/ItemModel')(a);
    userModel = require('../models/UserModel')(a);
    friendshipModel = require('../models/FriendshipModel')(a);
    commentModel = require('../models/CommentModel')(a);
    entity = a;
};

var config = require('../config/config')[process.env.NODE_ENV];

//deals with cloud image uploading
var uploadImage = require('./utils/uploadImage');
var cloudinary = require('cloudinary');
var scale = require('./utils/zoomCookie');

var msgpage = require('./utils/msgPages');

var ph = require('../lib/passwordHashing');

var Controller = {
    //Get methods
    get: {
        // '/u/:url'
        getUserByUrl: function(req, res) {
            var userUrl = req.params.url;
            if (userUrl.length === 0) return res.redirect('/');
            var returnObject = {
                isLogged: !!(req.user) ? true : false,
                targetType: null,
                page: 'view',
                load: {
                    user: null,
                    comments: [],
                    items: []
                },
                isFriend: false,
                comment: {
                    toType: 'users',
                    toId: null,
                    toUrl: null
                },
                user: req.user || null,
                image : cloudinary.image,
                useragent : req.useragent,
                url : config.domain,
                scale : scale.getCookie(req)
            };
            var allow = true;
            userModel.getUserByUrl(userUrl).then(function(a) {
                    returnObject.load.user = a;
                    returnObject.comment.toId = a.id;
                    returnObject.comment.toUrl = a.url;
                    returnObject.targetType = a.Seller ? 'service' : 'users';
                    if (returnObject.targetType === 'service') {
                        console.log('\n', 'check step:');
                        return itemsModel.getItemByServiceIdWithFullInfo(a.Seller.id);
                    }
                    return;
                }, function(n) {
                    allow = false;
                    return;
                }
            ).then(function(a) {
                    returnObject.load.items = a || [];
                    return commentModel.getCommentsByUserId(returnObject.load.user.id);
                }, function(n) {
                    allow = false;
                    return;
                }
            ).then(function(a) {
                    returnObject.load.comments = a || [];
                    if (!!returnObject.isLogged) return friendshipModel.isAddedInList(returnObject.user.id, returnObject.load.user.id);
                    else return null;
                }, function(n) {
                    allow = false;
                    return;
                }
            ).then(function(a) {
                    returnObject.isFriend = a || false;
                    return;
                }, function(n) {
                    allow = false;
                    return;
                }
            ).then(function(a) {
                if (allow) return res.render('users', returnObject);
                // return res.redirect('/');
                return msgpage.send(req, res,
                    'warning',
                    'Esse usuário não foi encontrado.',
                    'Usuário ' + userUrl + ' não foi encontrado',
                    '/');
            });
        }
    },

    //Post Methods
    post: {
        editUser: function(req, res) {
            if (!req.user) return res.redirect('/');
            var name = req.body.editname || null;
            var email = req.body.editemail || null;
            var password = req.body.editpass || null;
            var description = req.body.editdesc || null;
            var currentType = req.user.Seller ? 'seller' : 'buyer';
            var filename = null;

            var id = req.body.confid || null;

            if (id !== null) {
                id = Number(id);
            }

            var pass = req.body.confpass || null;
            if (!id || !pass) return msgpage.send(req, res, 'danger', 'Senha para efetuar modificação está incorreta', 'Verifique sua senha', '/portal/options');
            if (req.user.id !== id) return msgpage.send(req, res, 'danger', 'Houve algum erro', 'Por favor verifique os dados', '/portal/options');
            if (req.user.userpassword !== ph.encrypt(pass)) return msgpage.send(req, res, 'danger', 'Senha para efetuar modificação está incorreta', 'Verifique sua senha', '/portal/options');
            // id, pass, email, name, password, description, filename
            console.log(id, pass, email, name, password, description, filename);

            uploadImage(req.file).then(function(a){
                    filename = a.public_id;
                    if(currentType === 'buyer')
                        return buyersModel.updateBuyerDetails(id, pass, email, name, password, filename);
                    else if(currentType==='seller')
                        return sellerModel.updateSellerDetails(id, pass, email, name, password, description, filename);
                }, function (a){
                    // id, password, new_email, new_name, new_password, new_description, new_photo
                    if(currentType === 'buyer')
                        return buyersModel.updateBuyerDetails(id, pass, email, name, password);
                    else if(currentType==='seller')
                        return sellerModel.updateSellerDetails(id, pass, email, name, password, description);
                }
            ).then(function(a) {
                    // return res.redirect('/portal/options');
                    console.log(a);
                    return msgpage.send(req, res,
                        'success',
                        'Modificação de sua conta foi feito com sucesso',
                        'Conta modificada com sucesso',
                        '/portal/options');
                }, function(a) {
                    // return res.redirect('/portal/options');
                    console.log(a);
                    return msgpage.send(req, res,
                        'danger',
                        'Modificação de sua conta não foi feito. Por favor, verifique seus dados e tente novamente.',
                        'Conta não foi modificada',
                        '/portal/options');
                }
            );
        }
    }
};

module.exports = function(ent) {
    setEntity(ent);
    return Controller;
};
