var util = require('util');
// var servM = require('../models/ServiceModel');
var rootPath = require('../config/config')[process.env.NODE_ENV].rootPath;

var addressModel;
var entity;
var setEntity = function(a){
    addressModel = require('../models/AddressModel')(a);
    entity = a;
};
var scale = require('./utils/zoomCookie');

var msgpage = require('./utils/msgPages');

var Controller = {
    get : {
        createAddress : function(req, res){
            if (!req.user) return res.redirect('/');
            var returnObject = {
                user: req.user,
                isLogged : true,
                page : 'create',
                scale : scale.getCookie(req)
            };

            return res.render('address', returnObject);
        },
        updateAddress : function(req, res){
            if (!req.user) return res.redirect('/');
            if (!req.params.id) return res.redirect('/');
            if (Number(req.params.id)===0) return res.redirect('/');

            var returnObject = {
                user: req.user,
                isLogged : true,
                page : 'edit',
                scale : scale.getCookie(req)
            };
            console.log('\n\n',Number(req.params.id),'\n\n');
            addressModel.getAddressByIdAndUser(Number(req.params.id), req.user.id).then(function(a){
                    returnObject.address = a;
                    if(a === null) return res.redirect('/portal/addresses');
                    return res.render('address', returnObject);
                },function(a){
                    return res.redirect('/portal/addresses');
                }
            );
        },
        deleteAddress : function(req, res){
            if (!req.user) return res.redirect('/');
            var addressId = Number(req.params.addressId);
            var id = req.user.id;
            addressModel.removeAddressById(addressId, id).then(function(a){
                    return msgpage.send(req, res, 'success', 'O seu endereço foi deletado com sucesso.', 'Endereço deletado', '/portal/addresses');
                },
                function(a){
                    return msgpage.send(req, res, 'danger', 'Um erro ocorreu, pois o seu endereço não foi deletado.', 'Endereço não foi deletado', '/portal/addresses');
                }
            );
        }
    },
    post : {
        createAddress : function(req, res){
            if (!req.user) return res.redirect('/');
            //prepare the data
            var id = req.user.id;
            var inserterId = id,
                zip = req.body.regz,
                srt1 = req.body.rega1,
                country = req.body.regcount,
                state = req.body.regst,
                city = req.body.regct,
                num = !!req.body.regnum ? Number(req.body.regnum) : null,
                apt = !!req.body.regapt ? Number(req.body.regapt) : null,
                long = !!req.body.reglong ? Number(req.body.reglong) : null,
                lat = !!req.body.reglat ? Number(req.body.reglat) : null;

            //attempt to insert data
            addressModel.createNewAddress(inserterId, zip, srt1, country, state, city, num, apt, long, lat).then(function(a){
                return msgpage.send(req, res, 'success', 'Seu endereço foi criado com sucesso!', 'Endereço Criado', '/portal/addresses');
            }, function(a){
                return msgpage.send(req, res, 'danger', 'Seu endereço não foi criado. Por favor verifique seus dados.', 'Endereço não foi criado', '/portal/addresses');
            });
        },
        updateAddress : function(req, res){
            if (!req.user) return res.redirect('/');
            //prepare the data
            var id = req.user.id;
            var inserterId = id,
                addressId = req.body.editid,
                zip = req.body.editz,
                srt1 = req.body.edita1,
                country = req.body.editcount,
                state = req.body.editst,
                city = req.body.editct,
                num = !!req.body.editnum ? Number(req.body.editnum) : null,
                apt = !!req.body.editapt ? Number(req.body.editapt) : null,
                long = !!req.body.editlong ? Number(req.body.editlong) : null,
                lat = !!req.body.editlat ? Number(req.body.editlat) : null;

            //attempt to insert data
            addressModel.fixedAddressUpdate(inserterId, addressId, zip, srt1, country, state, city, num, apt, long, lat).then(function(a){
                return msgpage.send(req, res, 'success', 'Seu endereço foi atualizado com sucesso.', 'Endereço atualizado', '/portal/addresses');
            }, function(a){
                return msgpage.send(req, res, 'danger', 'Seu endereço não foi atualizado. Por favor verifique seus dados.', 'Endereço não foi atualizado', '/portal/addresses');
            });
        }
    }
};

module.exports = function(ent){
    setEntity(ent);
    return Controller;
};
