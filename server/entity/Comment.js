module.exports = function(sequelize, Sequelize){
    var Comment = sequelize.define('Comment', {
        content : {
            type : Sequelize.STRING(500),
            allowNull : false
        },
        id : {
            primaryKey : true,
            type : Sequelize.BIGINT,
            autoIncrement : true,
        }
    }, {
        paranoid : true
    });

    return Comment;
};
