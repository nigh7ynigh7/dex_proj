module.exports = function(sequelize, Sequelize){
    var Request = sequelize.define('Request', {
        completionDate : {
            type : Sequelize.DATE,
            validate : {
                isAfter : new Date().toString()
            }
        },
        startDate : {
            type : Sequelize.DATE,
            validate : {
                isAfter : new Date().toString()
            }
        },
        userEvaluation : {
            type : Sequelize.INTEGER,
            validate : {
                min : -1,
                max : 1
            }
        },
        userEvaluationComment : Sequelize.STRING(500),
        userEvaluationDate : Sequelize.DATE,
        serviceEvaluation : {
            type : Sequelize.INTEGER,
            validate : {
                min : -1,
                max : 1
            }
        },
        serviceEvaluationComment : Sequelize.STRING(500),
        serviceEvaluationDate : Sequelize.DATE,
        generatedNumber : {
            type : Sequelize.INTEGER,
            validate : {
                min : 0,
                max : 100000
            }
        },
        inputGeneratedValue : {
            type : Sequelize.INTEGER,
            validate : {
                min : 0,
                max : 100000
            }
        },
        payment : {
            type : Sequelize.STRING,
            allowNull : false
        },
        isRejected : {
            type : Sequelize.BOOLEAN,
            defaultValue : false,
            allowNull : false
        }

    }, {
        paranoid : true
    });

    return Request;
};
