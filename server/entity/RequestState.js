module.exports = function(sequelize, Sequelize){
    var RequestState = sequelize.define('RequestState', {
        description : {
            type : Sequelize.STRING(500),
            allowNull : false
        }
    }, {
        paranoid : true
    });

    return RequestState;
};
