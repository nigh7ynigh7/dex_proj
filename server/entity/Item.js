module.exports = function(sequelize, Sequelize){
    var Item = sequelize.define('Item', {
        itemName : {
            type : Sequelize.STRING(100),
            allowNull : false,
            validate : {
                len : [3]
            }
        },
        itemPrice : {
            type : Sequelize.DOUBLE,
            allowNull : false,
            defaultValue : 0,
            validate : {
                min : 0
            }
        },
        discount : {
            type : Sequelize.DOUBLE,
            allowNull : false,
            defaultValue : 0,
            validate : {
                min : 0
            }
        },
        picturename: Sequelize.STRING,
        description: Sequelize.STRING(500)
    }, {
        paranoid : true,
        getterMethods : {
            getDiscountValue : function(){
                return this.discount/100;
            }
        }
    });

    return Item;
};
