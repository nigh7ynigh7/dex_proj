module.exports = function(sequelize, Sequelize){
    var Buyer = sequelize.define('Buyer', {
        birthDate : {
            type : Sequelize.DATE,
        }
    }, {
        paranoid : true,
        createdAt : 'registrationDate'
    });

    return Buyer;
};
