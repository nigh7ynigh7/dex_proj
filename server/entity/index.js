// 'use strict';

var env       = process.env.NODE_ENV || 'development';
var config    = require('../config/config')[env];
var Sequelize = require('sequelize');

// var sequelize = new Sequelize(config.db);
//, {logging: false}
var sequelize = new Sequelize(require('../config/config')[process.env.NODE_ENV].db, {logging: false});
var db        = {};

// Fill model pls!

var Address = require('./Address')(sequelize, Sequelize);
var Agenda = require('./Agenda')(sequelize, Sequelize);
var Buyer = require('./Buyer')(sequelize, Sequelize);
var Comment = require('./Comment')(sequelize, Sequelize);
var CommentEval = require('./CommentEval')(sequelize, Sequelize);
var Item = require('./Item')(sequelize, Sequelize);
var ItemRequest = require('./ItemRequest')(sequelize, Sequelize);
var Product = require('./Product')(sequelize, Sequelize);
var Request = require('./Request')(sequelize, Sequelize);
var RequestState = require('./RequestState')(sequelize, Sequelize);
var Seller = require('./Seller')(sequelize, Sequelize);
var User = require('./User')(sequelize, Sequelize);
var Ownership = require('./Ownership')(sequelize, Sequelize);
var Friendship = require('./Friendship')(sequelize, Sequelize);

//associations

//inheritances simualtions
Product.belongsTo(Item, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});
Agenda.belongsTo(Item, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});
Item.hasOne(Agenda, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});
Item.hasOne(Product, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});

Seller.belongsTo(User, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});
Buyer.belongsTo(User, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});
User.hasOne(Seller, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});
User.hasOne(Buyer, {
    foreignKey: {
        allowNull : false,
        unique : true
    }
});

//not null foreign keys 1:1 or 1:N
//address and user
Address.belongsTo(User, {
    foreignKey : {
        allowNull : false
    }
});
User.hasMany(Address, {
    foreignKey : {
        allowNull : false
    }
});
//buyer and seller and request
Buyer.hasMany(Request, {
    foreignKey : {
        allowNull : false
    }
});
Seller.hasMany(Request, {
    foreignKey : {
        allowNull : false
    }
});
Request.belongsTo(Seller, {
    foreignKey : {
        allowNull : false
    }
});
Request.belongsTo(Buyer, {
    foreignKey : {
        allowNull : false
    }
});
//state and request
RequestState.hasMany(Request, {
    foreignKey : {
        allowNull : false
    }
});
Request.belongsTo(RequestState, {
    foreignKey : {
        allowNull : false
    }
});
//address(user) and request
Address.hasMany(Request, {
    as : "BuyerAddress",
    foreignKey : {
        allowNull : false,
        name : 'BuyerAddressId'
    }
});
Request.belongsTo(Address, {
    as : "BuyerAddress",
    foreignKey : {
        allowNull : false,
        name : 'BuyerAddressId'
    }
});

//seller and item
Seller.hasMany(Item, {
    foreignKey: {
        allowNull:false
    }
});
Item.belongsTo(Seller, {
    foreignKey: {
        allowNull:false
    }
});

Request.belongsTo(Address, {
    as : 'SellerAddress',
    foreignKey : {
        name : 'BuyerAddressId'
    }
});
Address.hasMany(Request, {
    as : 'SellerAddress',
    foreignKey : {
        name : 'BuyerAddressId'
    }
});

// simulated N:M for comments
User.hasMany(Comment, {
    as : 'Commenter',
    foreignKey : {
        allowNull : false,
        name : 'CommenterId'
    }
});
Comment.belongsTo(User, {
    as : 'Commenter',
    foreignKey : {
        allowNull : false,
        name : 'CommenterId'
    }
});
User.hasMany(Comment, {
    as : 'Commented',
    foreignKey : {
        allowNull : false,
        name : 'CommentedId'
    }
});
Comment.belongsTo(User, {
    as : 'Commented',
    foreignKey : {
        allowNull : false,
        name : 'CommentedId'
    }
});

//smiluated N:M for friend
User.hasMany(Friendship, {
    as : "Friend",
    foreignKey : {
        name : 'UserTo',
        allowNull : false,
    }
});
User.hasMany(Friendship, {
    as : "Befriended",
    foreignKey : {
        name : 'UserFrom',
        allowNull : false,
    }
});
Friendship.belongsTo(User, {
    as : "Friend",
    foreignKey : {
        name : 'UserTo',
        allowNull : false,
    }
});
Friendship.belongsTo(User, {
    as : "Befriended",
    foreignKey : {
        name : 'UserFrom',
        allowNull : false,
    }
});

//M:N relationships

Item.belongsToMany(Request, {
    through : ItemRequest
});
Request.belongsToMany(Item, {
    through : ItemRequest
});

User.belongsToMany(Comment, {
    as : "Voter",
    through : CommentEval
});
Comment.belongsToMany(User, {
    as : "Comment",
    through : CommentEval
});

Buyer.belongsToMany(Seller, {
    as : "Owned",
    through : Ownership
});
Seller.belongsToMany(Buyer, {
    as : "Owner",
    through : Ownership
});
//adds items to entity

db.Address = Address;
db.Agenda = Agenda;
db.Buyer = Buyer;
db.Comment = Comment;
db.CommentEval = CommentEval;
db.Item = Item;
db.ItemRequest = ItemRequest;
db.Product = Product;
db.Request = Request;
db.RequestState = RequestState;
db.Friendship = Friendship;
db.Seller = Seller;
db.User = User;

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
// sequelize.sync({force:true});
