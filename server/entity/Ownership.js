module.exports = function(sequelize, Sequelize){
    var Ownership = sequelize.define('Ownership', {
        OwnershipLevel : {
            type : Sequelize.DOUBLE,
            allowNull : false,
            defaultValue : 0,
            validate : {
                min : 0
            }
        },
        isOwner : {
            type : Sequelize.BOOLEAN,
            allowNull : false,
            defaultValue : true
        }
    }, {
        paranoid : true,
        getterMethods : {
            getDiscountValue : function(){
                return this.discount/100;
            }
        }
    });

    return Ownership;
};
