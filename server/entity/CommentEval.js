module.exports = function(sequelize, Sequelize){
    var CommentEval = sequelize.define('CommentEval', {
        point : {
            type : Sequelize.INTEGER,
            allowNull : false,
            validate : {
                min : -1,
                max : 0
            }
        }
    }, {
        paranoid : true
    });

    return CommentEval;
};
