module.exports = function(sequelize, Sequelize){
    var ItemRequest = sequelize.define('ItemRequest', {
        itemPrice : {
            type : Sequelize.DOUBLE,
            allowNull : false,
            defaultValue : 0,
            validate : {
                min : 0
            }
        },
        discount : {
            type : Sequelize.DOUBLE,
            allowNull : false,
            defaultValue : 0,
            validate : {
                min : 0
            }
        },
        quantity : {
            type : Sequelize.DOUBLE,
            allowNull : false,
            defaultValue : 0,
            validate : {
                min : 1
            }
        }
    }, {
        paranoid : true,
        getterMethods : {
            getDiscountValue : function(){
                return this.discount/100;
            }
        }
    });

    return ItemRequest;
};
