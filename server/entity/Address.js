module.exports = function(sequelize, Sequelize){
    var Address = sequelize.define('Address', {
        address : {
            type : Sequelize.STRING,
            allowNull : false
        },
        number : {
            type : Sequelize.INTEGER,
            allowNull : false,
            validate : {
                min : 0
            }
        },
        aptNumber : {
            type : Sequelize.INTEGER,
            validate : {
                min : 0
            }
        },
        zip : {
            type : Sequelize.STRING,
            allowNull : false
        },
        city : {
            type : Sequelize.STRING,
            allowNull : false
        },
        state : {
            type : Sequelize.STRING,
            allowNull : false
        },
        country : {
            type : Sequelize.STRING,
            allowNull : false
        },
        latitude : {
            type : Sequelize.DOUBLE,
            validate : {
                min : -90,
                max : 90
            }
        },
        longitude : {
            type : Sequelize.DOUBLE,
            validate : {
                min : -180,
                max : 180
            }
        }
    }, {
        paranoid : true,
        timestamp : false,
        validate : {
            bothLatsOrNone : function(){
                if((this.latitude === null) !== (this.longitude === null)){
                    console.log('\n\n\n Shitty validation!');
                    throw new Error('Necessita-se de ambas cordenadas');
                }
            }
        }
    });

    return Address;
};
