module.exports = function(sequelize, Sequelize){
    var User = sequelize.define('User', {
        username : {
            type : Sequelize.STRING(100),
            allowNull : false,
            unique : true,
            validate : {
                len : [1]
            }
        },
        userpassword : {
            type : Sequelize.STRING(100),
            allowNull : false,
            validate : {
                len : [1]
            }
        },
        email : {
            type : Sequelize.STRING,
            allowNull : false,
            unique : true,
            validate : {
                isEmail : true
            }
        },
        url : {
            type : Sequelize.STRING,
            allowNull : false,
            unique : true,
            validate : {
                is : /^[a-zA-Z0-9\_\-]{1,30}$/g
            }
        },
        isVisible : {
            type : Sequelize.BOOLEAN,
            allowNull : false,
            defaultValue : true
        },
        isVerified : {
            type : Sequelize.BOOLEAN,
            allowNull : false,
            defaultValue : true
        },
        picturename: Sequelize.STRING
    }, {
        paranoid : true,
        createdAt : 'registrationDate'
    });

    return User;
};
