module.exports = function(sequelize, Sequelize){
    var Seller = sequelize.define('Seller', {
        description : {
            type : Sequelize.STRING,
        },
        concurrency : {
            type : Sequelize.INTEGER,
            defaultValue : 0,
            validate : {
                min : 0
            }
        },
        rest : {
            type : Sequelize.INTEGER,
            defaultValue : 0,
            validate : {
                min : 0
            }
        },
    }, {
        paranoid : true,
        createdAt : 'registrationDate'
    });

    return Seller;
};
