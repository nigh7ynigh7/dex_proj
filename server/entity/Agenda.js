module.exports = function(sequelize, Sequelize){
    var Agenda = sequelize.define('Agenda', {
        minDuration : {
            type : Sequelize.INTEGER,
            defaultValue : 0,
            allowNull : false,
            validate : {
                min : 0
            }
        },
        maxDuration : {
            type : Sequelize.INTEGER,
            defaultValue : 0,
            allowNull : false,
            validate : {
                min : 0
            }
        }
    }, {
        paranoid : true
    });

    return Agenda;
};
