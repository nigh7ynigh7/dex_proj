console.log('in');
console.log(addresses, items);

var attempt = 0;
app.controller('RequestController', ['$http','$scope', function($http, $scope){
    //recieved data
    $scope.addresses = addresses || [];
    $scope.items = items || [];
    $scope.type = type;
    //data receptors
    $scope.selectedIName = 'Escolhe um item';
    $scope.slectedIId = null;
    $scope.startDate = null;
    $scope.startHour = null;
    $scope.paym = null;
    //data manipulators
    $scope.selectedAddressId = null;
    $scope.selectedItems = [];
    $scope.activeItem = {};
    $scope.quantity = null;
    $scope.selectedItemType = '';
    //view manipulators
    $scope.currentStep = 1;
    $scope.locked = false;


    var advanceStepLogic = function(direction,current){
        if (direction === 'f') {
            if (current === 4) {
                return current;
            }else {
                return ++current;
            }
        } else if (direction === 'b') {
            if (current === 1) {
                return 1;
            }else {
                return --current;
            }
        }else {
            return current;
        }
    };
    this.advanceStep = function(direction){
        if ($scope.currentStep === 4) {
            return;
        }else {
            $scope.currentStep = advanceStepLogic(direction, $scope.currentStep);
            if ($scope.currentStep === 4) {
                $scope.locked = true;
            }
        }
    };
    this.compareSteps = function(step, panel){
        if (panel) {
            if (panel === 'panel') {
                return $scope.currentStep === step;
            }else {
                return $scope.currentStep === step || $scope.currentStep === 4;
            }
        }else {
            return $scope.currentStep === step || $scope.currentStep === 4;
        }
    };


    //deals with items
    this.setActiveItem = function(item){
        $scope.selectedIName = item.itemName;
        $scope.slectedIId = item.id;
        console.log(item);
        $scope.activeItem = item;
        $scope.selectedItemType = item.Agenda ? 'agenda' : 'goods';
    };
    this.addItem = function(quantity){
        if (!quantity || quantity === undefined) {
            return;
        }
        if ($scope.activeItem) {
            $scope.selectedItems.push({
                quantity : $scope.quantity,
                ItemId : $scope.activeItem.id,
                itemName : $scope.activeItem.itemName,
                itemPrice : $scope.activeItem.itemPrice,
                picturename : $scope.activeItem.picturename
            });
            $scope.activeItem = null;
            $scope.quantity = 0;
            $scope.selectedIName = 'Escolha um item';
        }else {
            return;
        }
    };
    this.getMinFromItem = function(){

        if ($scope.activeItem === {} || $scope.activeItem === null) return null;
        var active = $scope.activeItem;
        var value = active.Agenda.minDuration;
        if($scope.quantity === 0 || !$scope.quantity) $scope.quantity = active.Agenda.minDuration;
        if (value !== null) {
            return value;
        }else {
            return 0;
        }
    };
    this.getMaxFromItem = function(){
        if ($scope.activeItem === {} || $scope.activeItem === null) return null;
        var active = $scope.activeItem;
        var value = active.Agenda.maxDuration;
        if (value !== null) {
            if (Number(value) === 0) {
                return null;
            }
            return value;
        }else {
            return null;
        }
    };

    //deals with addresses:
    this.getClassIfOn = function(id){
        return $scope.selectedAddressId === id ? 'btn-primary' : 'btn-default';
    };
    this.setAddress = function(id){
        $scope.selectedAddressId = id;
    };
    this.showButtonText = function(id){
        return $scope.selectedAddressId === id ? 'Selecionado' : 'Selecione';
    };

    //deals with extra options
    var getMSFromDate = function(){
        var date = (new Date($scope.startDate).getTime()) + (Number($scope.startHour)*60*60*1000);
        console.log(date);
        if (date > (new Date().getTime())) {
            return date;
        }else {
            return null;
        }
    };

    //send to server
    this.sendToServer = function(){
        if($scope.selectedAddressId === null) return;
        if($scope.selectedItems.length === 0) return;
        if (service_id === null) return;
        var sendObject = {
            address : $scope.selectedAddressId,
            service : service_id,
            items : $scope.selectedItems
        };

        if ($scope.startDate && $scope.startHour) sendObject.startDate = getMSFromDate();

        if ($scope.paym) sendObject.payment = $scope.paym;
        console.log(sendObject);
        $http.post('/request/create/', sendObject).success(function(data){
            if(data.success){
                toastr.success('Seu pedido foi realizado com sucesso. Espere um pouco e você será redirecionado até a página do pedido.', 'Pedido realizado');
                setTimeout(function(){
                    console.log('Parabéns!');
                    window.location = data.redirect;
                }, 5000);
            }else {
                if (attempt > 3)
                    toastr.error('Parece que está acontecendo um erro. Verifique os dados do pedido.', 'Erro');
                else
                    toastr.error('Você tentou mandar o pedido muitas vezes. Pode ser que você ainda está faltando algo. Mas, também, pode ser que o problema é nosso; tente mais uma vez em alguns instantes. ', 'Espera um pouco e tente mais tarde.');
            }
        });
    };
}]);
